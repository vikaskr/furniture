<?php

    use yii\helpers\Url;        
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yz\shoppingcart\ShoppingCart;

    $user = new \app\models\MubUser();
    $booking = new \app\models\Booking();
    $userContact = new \app\models\MubUserContact();
    if(!Yii::$app->user->isGuest){
    $mubId = Yii::$app->user->getId();
    $users = $user::find()->where(['user_id' => $mubId])->one();
    $id = $users->id;
    $userDetails = $userContact::find()->where(['mub_user_id' => $id])->one();}
    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();
 
    ?>

<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Checkout</h2>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="index.html"><span class="icon icon-home"></span></a></li>
                <li><a href="checkout-1.html">Cart items</a></li>
                <li><a class="active" href="checkout-2.html">Delivery</a></li>
                <li><a href="checkout-3.html">Payment</a></li>
            </ol>
        </div>
    </header>
</section>


<section class="checkout">
<div class="container">

<div class="cart-wrapper">

<div class="note-block">
<div class="row">

<!-- === left content === -->
<?php if(Yii::$app->user->isGuest){?>
<div class="col-md-3"></div>
<div class="col-md-6">
  <div class="row" style="margin-top: 3em;">
      <div class="h4 col-md-6" style="text-align: center;">Register now</div>
      <div class="col-md-6" style="text-align: center; margin-top: -10px;">
      <a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal" class="btn btn-main"> Login/Register</a>
      </div>
  </div>
  <div class="h6" style="text-align: center;">('Login' or 'register' for Proceed Payments Option)</div>
   <hr />
</div>
  <?php } else{?>

<div class="col-md-6">

<!-- === login-wrapper === -->

<div class="login-wrapper">

    <div class="white-block">
        

        <div class="login-block login-block-signup">

            

              <?php
                 if(!Yii::$app->user->isGuest){
                 $mubUserId = \app\models\User::getMubUserId();
                 $mubUserModel = new \app\models\MubUser();
                 $currentUser = $mubUserModel::findOne($mubUserId);
                 $userDetails = $currentUser;
                 $contactDetails = $currentUser->mubUserContacts;

                 $tokenAmt = $cart->getCost(); 
                                // p($amount);
                  // Merchant key here as provided by Payu
                  //payu test key : rjQUPktU , Salt: e5iIg1jwi8
                  $MERCHANT_KEY = "bstnPW9E";

                  // Merchant Salt as provided by Payu
                  $SALT = "AJi6ZxOUP2";

                  // End point - change to https://secure.payu.in for LIVE mode
                  $PAYU_BASE_URL = "https://secure.payu.in";

                  $formError = 0;
                  $action = $PAYU_BASE_URL . '/_payment';
                  foreach($cartItems as $value){
                    $itemCartName = $value['product_name'];
                    }

                $form = ActiveForm::begin(['options' => ['id' => 'confirm-payment','name' => 'payuForm','method' => 'POST'],'action' => $action]);
                 $booking = new \app\models\Booking();
                 ?>
                         
                          <input type="hidden" id="key" name="key" value="<?=$MERCHANT_KEY;?>">
                          <input type="hidden" id="price" name="amount" value="<?= $tokenAmt;?>">
                          <input type="hidden" name="txnid" value="<?=$currentUser->username.'_'.time();?>" />
                          <input type="hidden" id="product" name="product" value="<?= isset($itemCartName)? $itemCartName: '';?>">
                          <input type="hidden" id="firstname" name="firstname" value="<?= $currentUser->first_name?>">
                          <input type="hidden" id="email" name="email" value="<?=$contactDetails->email;?>">
                          <input type="hidden" id="phone" name="phone" value="<?=$contactDetails->mobile;?>">

                          <input type="hidden" id="lastname" name="lastname" value="<?= $currentUser->last_name;?>">
                          <input type="hidden" id="country" name="country" value="<?= $contactDetails->country;?>">
                          <input type="hidden" id="pin_code" name="pin_code" value="<?= $contactDetails->pin_code;?>">
                           
                          <input type="hidden" id="quantity" name="quantity" value=" <?= $value->getQuantity();?>">
                          <input type="hidden" id="city" name="city" value="<?= isset($contactDetails->city)? $contactDetails->city: '';?>">
                          <input type="hidden" id="state" name="state" value="<?= isset($contactDetails->state)? $contactDetails->state: '';?>">
                          <input type="hidden" id="surl" name="surl" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/payu/success.php';?>">
                          <input type="hidden" id="furl" name="furl" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/payu/failure.php';?>">
                          <input type="hidden" id="udf1" name="udf1" value="">
                          <input type="hidden" id="udf2" name="udf2" value="">
                          <input type="hidden" id="udf3" name="udf3" value="">
                          <input type="hidden" id="udf4" name="udf4" value="">
                          <input type="hidden" id="udf5" name="udf5" value="">
                          <input type="hidden" id="udf6" name="udf6" value="">
                          <input type="hidden" id="udf7" name="udf7" value="">
                          <input type="hidden" id="udf8" name="udf8" value="">
                          <input type="hidden" id="udf9" name="udf9" value="">
                          <input type="hidden" id="udf10" name="udf10" value="">
                          <input type="hidden" id="service_provider" name="service_provider" value="payu_paisa">
                         <br /><br />
                         
                                  
                
            <div class="row" style="margin-top: -43px;">
                <div class="h4" style="text-align: center;">Delivery Address</div>
                   <hr />

                <div class="col-md-6">
                    <div class="form-group">
                         <?= $form->field($booking, 'first_name')->textInput(['readOnly'=> true, 'value' => $currentUser->first_name, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($booking, 'last_name')->textInput(['readOnly'=> true, 'value' => $currentUser->last_name, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($booking, 'email')->textInput(['readOnly'=> true, 'value' => $contactDetails->email, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                         <?= $form->field($booking, 'mobile')->textInput(['readOnly'=> true, 'value' => $contactDetails->mobile, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($booking, 'city')->textInput(['readOnly'=> true, 'value' => $contactDetails->city, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($booking, 'state')->textInput(['readOnly'=> true, 'value' => $contactDetails->state, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($booking, 'pin_code')->textInput(['readOnly'=> true, 'value' => $contactDetails->pin_code, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($booking, 'country')->textInput(['readOnly'=> true, 'value' => $contactDetails->country, 'placeholder' => "Name", 'class' => 'form-control']); ?>
                    </div>
                </div>

                 <div class="col-md-12">
                    <div class="form-group">
                         <?= $form->field($booking, 'delivery_address')->textarea(['placeholder' => " Product Delivery Address",'name' => 'delivery_address','id' => 'delivery_address', 'class' => 'form-control', 'rows' => "4"]); ?>
                    </div>
                </div>

                           
            </div>
                <hr />
            
        </div> <!--/signup-->
    </div>
</div> <!--/login-wrapper-->
</div> <!--/col-md-6-->
<!-- === right content === -->

<div class="col-md-6">

<div class="white-block">

    <div class="h4" style="text-align: center;">Your Product</div>

    <hr />

        <div class="cart-wrapper">
            <!--cart header -->

            <div class="cart-block cart-block-header">
                <div style="text-align: left;">
                    <span>Product</span>
                </div>
                <div style="text-align: center;">
                    <span>Quantity</span>
                </div>
                <div style="text-align: left;">
                    <span>Price</span>
                </div>
            </div>

            <!--cart items-->
<?php foreach($cartItems as $value){
        $productId = $value['id'];?>
            <div class="clearfix">
                
                <div class="cart-block cart-block-item" style="margin-top: 15px;">
                    <div class="title">
                        <div class="h6"><a href="/site/product?name=<?= $value['product_slug'];?>"><?=$value['product_name'];?></a></div>
                    </div>
                    <div class="quantity">
                        <strong><?= $value->getQuantity();?></strong>
                    </div>
                    <div class="price">
                        <span class="final h6">₹ <?= ($value['mrp'])*($value->getQuantity());?></span>
                    </div>
                </div>
                 <hr />
            </div>


           
        </div>
    <?php } ?> <div class="clearfix">
                <div class="cart-block cart-block-footer clearfix">
                    <div>
                        <strong>Total Payments</strong>
                    </div>
                    <div>
                        <div class="h2 title"> ₹ <?= $cart->getCost();?></div>
                    </div>
                </div>
            </div>
            <hr />

 <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8" style="margin-top: 3em;">
        <input type="submit" value="Continue to Payment" formtarget="_blank" class="btn btn-main">
      <!--   <input type="button" value="Cash On Delivery" id="codelivery" class="btn btn-main" style="background: grey; margin-left: 10px;"><br/> -->
        </div>
 </div>
</div>
<?php ActiveForm::end();?>
            <?php }}?>
</div>
</div>
</div>

              
  </section>


