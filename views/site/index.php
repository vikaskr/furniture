<?php 

use app\helpers\ImageUploader;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$product = new \app\modules\MubAdmin\modules\furniture\models\Product();
$productDetails = $product::find()->where(['del_status' => '0'])->limit(6)->all();

$category = new \app\modules\MubAdmin\modules\furniture\models\Category();
$categoryDetails = $category::find()->where(['del_status' => '0'])->orderby(['id' => SORT_ASC])->limit(12)->all();
$categoryDetail = $category::find()->where(['del_status' => '0'])->orderby(['id' => SORT_DESC])->limit(12)->all();
$categoryLiving = $category::find()->where(['del_status' => '0', 'category_name' => 'living'])->all();
$categorySofa = $category::find()->where(['del_status' => '0', 'category_name' => 'sofa'])->all();
$categoryBedroom = $category::find()->where(['del_status' => '0', 'category_name' => 'bedroom'])->all();
$categoryDining = $category::find()->where(['del_status' => '0', 'category_name' => 'dining'])->all();



$this->title = 'Furniture Minds';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
@media (max-width: 767px) {
    .popup-main .popup-table .popup-cell .icon {
        font-size: 20px;
    }
    .pad{
        padding: 10px!important;
    }
}

figure {
  margin: 0;
  padding-bottom: 6px!important;
    padding-left: 23px!important; }
.f-icon {
    font-size: 90px!important;
}
.fig{
    font-size: 13px!important;
}
.fon{
    font-size: 13px!important;
    margin-left: 0em!important;
}
.fon2{
    font-size: 13px!important;
    color: #fff!important;
}
.padtop{
    padding-top: 0px!!important;
}
.pad{e
    padding: 20px!!important;
}
.padb{
    margin-top: -20px!!important;
    padding-bottom: 20px!!important;  
}
.padt{
     border-top: 1px solid #cacaca;
     margin-top: -25px!important;
     border-bottom: 1px solid #cacaca!important;
}
.bord{
    border-right: 1px solid #cacaca!important;
}</style>

        <section class="header-content">

            <div class="owl-slider">

                <!-- === slide item === -->

                <div class="item" style="background-image:url(/images/img2.jpg)">
                    <div class="box">
                        <div class="container">
                            <h2 class="title animated h1" data-animation="fadeInDown">Modern furniture theme</h2>
                            <div class="animated" data-animation="fadeInUp">
                                Modern & powerfull template. <br /> Clean design & reponsive
                                layout. Google fonts integration
                            </div>
                           <!--  <div class="animated" data-animation="fadeInUp">
                                <a href="https://themeforest.net/item/mobel-furniture-website-template/20382155" target="_blank" class="btn btn-main" ><i class="icon icon-cart"></i> Buy this template</a>
                            </div> -->
                        </div>
                    </div>
                </div>

                <!-- === slide item === -->

                <div class="item" style="background-image:url(/images/img3.jpg)">
                    <div class="box">
                        <div class="container">
                            <h2 class="title animated h1" data-animation="fadeInDown">Mobile ready!</h2>
                            <div class="animated" data-animation="fadeInUp">Unlimited Choices. Unbeatable Prices. Free Shipping.</div>
                            <div class="animated" data-animation="fadeInUp">Furniture category icon fonts!</div>
                           <!--  <div class="animated" data-animation="fadeInUp">
                                <a href="category.html" class="btn btn-clean">Get insipred</a>
                            </div> -->
                        </div>
                    </div>
                </div>

                <!-- === slide item === -->

                <div class="item" style="background-image:url(/images/img.jpg)">
                    <div class="box">
                        <div class="container">
                            <h2 class="title animated h1" data-animation="fadeInDown">
                                Very Animate.css Friend.
                            </h2>
                            <div class="desc animated" data-animation="fadeInUp">
                                Combine with animate.css. Or just use your own!.
                            </div>
                            <div class="desc animated" data-animation="fadeInUp">
                                Bunch of typography effects.
                            </div>
                            <!-- <div class="animated" data-animation="fadeInUp">
                                <a href="https://themeforest.net/item/mobel-furniture-website-template/20382155" target="_blank" class="btn btn-clean">Buy this template</a>
                            </div> -->
                        </div>
                    </div>
                </div>

            </div> <!--/owl-slider-->
        </section>

        <!-- ========================  Icons slider ======================== -->

        <section class="owl-icons-wrapper owl-icons-frontpage">

            <!-- === header === -->

            <header class="hidden">
                <h2>Product categories</h2>
            </header>

            <div class="container">

                <div class="owl-icons">

                    <!-- === icon item === -->
                     <?php if(!empty($categoryDetails)){
                     foreach($categoryDetails as $value){?>
                    <a href="/site/category?name=<?= $value['category_slug']?>">
                        <figure>
                            <i class="f-icon <?= $value['icon_name']?>" style="color: #ffbb00;"></i>
                            <figcaption style="color: #000; text-align: center;"><b class="fon2"><?= $value['category_name']?></b></figcaption>
                        </figure>
                    </a>
                    <?php }}?>
                </div> <!--/owl-icons-->
            </div> <!--/container-->
        </section>

        <!-- ========================  Products widget ======================== -->

        <section class="hed" style="padding-bottom: 0px!important;">
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10 text-center">
                            <h2 class="h2 title">Why Furniture Minds is Different</h2>
                            <div class="text">
                                <p>Browse our wide selection of Sofas, Beds, Dining Table Sets, Study table and more. Give your home an elegant touch and feel at affordable prices and avail all the exciting deals and discount offers on our online shopping store. You can buy furniture products carved out of solid wood material or choose from various designs and features to match your requirements.</p>
                            </div>
                               <h5> <span class="icon icon-magic-wand"></span> Premium Quality &nbsp; &nbsp; &nbsp;  <span class="icon icon-apartment"></span> Always On-Time&nbsp; &nbsp; &nbsp;  <span class="icon icon-home"></span> Free Delivery &nbsp; &nbsp; &nbsp; <span class="icon icon-pencil"></span> Easy Returns</h5>
                        </div>
                    </div>
                </div>
            </header>
        </section>
        

        <section class="products padtop" style="padding-top: 20px!important; background-color: #F3F3F4!important;">
            <div class="container">

                <header>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title">BROWSE POPULAR CATEGORIES</h2>
                           <!--  <div class="text">
                                <p>Select category</p>
                            </div> -->
                        </div>
                    </div>
                </header>

                <div class="row">         
                <?php if(!empty($categoryDetails)){
                foreach($categoryDetails as $value){?>
                    <div class="col-md-2 col-xs-4">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="image">
                                    <?php if(!empty($value['image'])){?>
                                    <a href="/site/category?name=<?= $value['category_slug']?>">
                                        <img src="<?= ImageUploader::resizeRender('/'.$value['image'], '263', '220'); ?>" alt="" />
                                    </a>
                                    <?php } else {?>
                                    <a href="/site/category?name=<?= $value['category_slug']?>">
                                        <img src="/images/not-found.png" height="138" width="165" alt="" />
                                    </a>
                                    <?php }?>
                                </div>
                                <div class="text textcent">
                                    <h2 class="title h4 fon"><a href="/site/category?name=<?= $value['category_slug']?>"><?= $value['category_name']?></a></h2>
                                </div>
                            </div>
                        </article>
                    </div>                    
                <?php }}?>

                </div><!--/row-->
       
            </div><!--/container-->
        </section>
       
        <section class="products padtop" style="padding-top: 20px!important;">
            <div class="container">

                <header>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title">TOP SELLINGS</h2>
                           
                        </div>
                    </div>
                </header>

                <div class="row">

                    <!-- === product-item === -->

                    <?php if(!empty($categorySofa)){

                    foreach ($categorySofa as $value) {
                       $bedroom = $value['category_name'];
                       $bedroomsl = $value['category_slug'];
                       $tagline = $value['tag_line'];
                       $description = $value['description'];
                       $image = $value['image'];
                       $id = $value['id'];
                    ?>
                    <div class="col-md-7 col-xs-12">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="text">
                                    <p><?= $tagline;?></p>
                                    <h2 class="title h4"><a href="/site/category?name=<?= $bedroomsl?>"><?= $bedroom;?></a></h2>
                                    <p><?= $description;?></p>
                                </div><br/><br/>
                                <div class="row padt">
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=fabric-sofa-sets"><figure>
                                            <i class="f-icon f-icon-sofa"></i>
                                            <figcaption class="fig">Fabric Sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=wooden-sofa-sets"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">Wooden Sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=3-seater-sofa"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">3 Seater Sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3">
                                        <a href="/site/category?name=2-seater-sofa"><figure>
                                            <i class="f-icon f-icon-chair"></i>
                                            <figcaption class="fig">2 Seater Sofa</figcaption>
                                        </figure></a>
                                    </div>
                                </div>

                                <div class="row padb" style="border-bottom: 1px solid #cacaca;">
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=leatherette-sofa"><figure>
                                            <i class="f-icon f-icon-sofa"></i>
                                            <figcaption class="fig">Leatherette Sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=chesterfield-sofas"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">Chesterfield Sofas</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=futonslove-seats"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">Futons/Love Seats</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3">
                                        <a href="/site/category?name=sectional-sofas"><figure>
                                            <i class="f-icon f-icon-chair"></i>
                                            <figcaption class="fig">Sectional Sofas</figcaption>
                                        </figure></a>
                                    </div><br/>
                                </div>
                            </div>
                        </article>
                    </div>

                    <!-- === product-item === -->

                    <div class="col-md-5 col-xs-12 marg-left">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="image">
                                     <?php if(!empty($image)){?>
                                    <a href="/site/category?name=<?= $bedroomsl?>">
                                        <img src="<?= ImageUploader::resizeRender('/'.$image, '652', '440'); ?>" alt="" height="363" />
                                    </a>
                                    <?php } else {?>
                                     <a href="/site/category?name=<?= $bedroomsl?>">
                                        <img src="/images/not-found.png" height="220" width="263" alt="" />
                                    </a>
                                    <?php }?>
                                </div>
                            </div>
                        </article>
                    </div>                   
                    
                <?php }}?>
                </div><!--/row-->
       
            </div><!--/container-->
        </section>
        <section class="products padtop" style="padding-top: 20px!important;">
            <div class="container">
                <div class="row">
                    <?php if(!empty($categoryLiving)){

                        foreach ($categoryLiving as $value) {
                           $bedroom2 = $value['category_name'];
                           $bedroom2sl = $value['category_slug'];
                           $tagline2 = $value['tag_line'];
                           $description2 = $value['description'];
                           $image2 = $value['image'];
                           $id2 = $value['id'];
                        ?>
                    <div class="col-md-5 col-xs-12 marg-right">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                 <?php if(!empty($image2)){?>
                                <div class="image">
                                    <a href="/site/category?name=<?= $bedroom2sl?>">
                                        <img src="<?= ImageUploader::resizeRender('/'.$image2, '652', '440'); ?>" alt="" height="363" />
                                    </a>
                                    <?php } else {?>
                                   <a href="/site/category?name=<?= $bedroom2sl?>">
                                        <img src="/images/not-found.png" height="220" width="263" alt="" />
                                    </a>
                                    <?php }?>
                                </div>
                            </div>
                        </article>
                    </div> 
                     <!-- === product-item === -->

                    <div class="col-md-7 col-xs-12">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="text">
                                    <p><?= $tagline2; ?></p>
                                    <h2 class="title h4"><a href="/site/category?name=<?= $bedroomsl?>"><?= $bedroom2; ?></a></h2>
                                    <p><?= $description2; ?></p>
                                </div><br/><br/>
                                <div class="row padt">
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-sofa"></i>
                                            <figcaption class="fig">f-icon-sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-chair"></i>
                                            <figcaption class="fig">f-icon-chair</figcaption>
                                        </figure></a>
                                    </div>
                                </div>

                                <div class="row padb" style="border-bottom: 1px solid #cacaca;">
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-sofa"></i>
                                            <figcaption class="fig">f-icon-sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-chair"></i>
                                            <figcaption class="fig">f-icon-chair</figcaption>
                                        </figure></a>
                                    </div><br/>
                                </div>
                            </div>
                        </article>
                    </div>                  
                    <?php }}?>
                </div><!--/row-->
       
            </div><!--/container-->
        </section>
        <section class="products padtop" style="padding-top: 20px!important;">
            <div class="container">
                <div class="row">

                   <?php if(!empty($categoryBedroom)){
                    foreach ($categoryBedroom as $value) {
                       $bedroom3 = $value['category_name'];
                       $bedroom3sl = $value['category_slug'];
                       $tagline3 = $value['tag_line'];
                       $description3 = $value['description'];
                       $image3 = $value['image'];
                       $id3 = $value['id'];
                                       ?>

                     <div class="col-md-7 col-xs-12">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="text">
                                    <p><?= $tagline3; ?></p>
                                    <h2 class="title h4"><a href="/site/category?name=<?= $bedroomsl?>"><?= $bedroom3; ?></a></h2>
                                    <p><?= $description3; ?></p>
                                </div><br/><br/>
                                <div class="row padt">
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-sofa"></i>
                                            <figcaption class="fig">f-icon-sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-chair"></i>
                                            <figcaption class="fig">f-icon-chair</figcaption>
                                        </figure></a>
                                    </div>
                                </div>

                                <div class="row padb" style="border-bottom: 1px solid #cacaca;">
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-sofa"></i>
                                            <figcaption class="fig">f-icon-sofa</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 bord">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-armchair"></i>
                                            <figcaption class="fig">f-icon-armchair</figcaption>
                                        </figure></a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3">
                                        <a href="/site/category?name=<?= $bedroomsl?>"><figure>
                                            <i class="f-icon f-icon-chair"></i>
                                            <figcaption class="fig">f-icon-chair</figcaption>
                                        </figure></a>
                                    </div><br/>
                                </div>
                            </div>
                        </article>
                    </div>

                    <!-- === product-item === -->

                    <div class="col-md-5 col-xs-12 marg-left">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="image">
                                     <?php if(!empty($image3)){?>
                                    <a href="/site/category?name=<?= $bedroomsl?>">
                                        <img src="<?= ImageUploader::resizeRender('/'.$image3, '652', '440'); ?>" alt="" height="363" />
                                    </a>
                                    <?php } else {?>
                                     <a href="/site/category?name=<?= $bedroomsl?>">   <img src="/images/not-found.png" height="220" width="263" alt="" />
                                    </a>
                                    <?php }?>
                                </div>
                            </div>
                        </article>
                    </div>                   
                <?php }}?>
                </div><!--/row-->
       
            </div><!--/container-->
        </section>

        <section class="products padtop" style="padding-top: 20px!important;">
            <div class="container">
                <div class="row">

                   <?php if(!empty($categoryDining)){
                    foreach ($categoryDining as $value) {
                       $bedroom3 = $value['category_name'];
                       $bedroom3sl = $value['category_slug'];
                       $tagline3 = $value['tag_line'];
                       $description3 = $value['description'];
                       $image3 = $value['image'];
                       $id3 = $value['id'];
                                       ?>
                     <div class="col-md-6 col-xs-12 marg-right">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="image">
                                     <?php if(!empty($image3)){?>
                                    <a href="/site/category?name=<?= $bedroomsl?>">
                                        <img src="<?= ImageUploader::resizeRender('/'.$image3, '652', '440'); ?>" alt="" height="400" />
                                    </a>
                                     <?php } else {?>
                                    <a href="/site/category?name=<?= $bedroomsl?>">
                                        <img src="/images/not-found.png" height="220" width="263" alt="" />
                                    </a>
                                    <?php }?>
                                </div>
                            </div>
                        </article>
                    </div> 
                    <div class="col-md-6 col-xs-12">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="text">
                                     <p><?= $tagline3 ?></p>
                                    <h2 class="title h4"><a href="/site/category?name=<?= $bedroom3sl?>"><?= $bedroom3 ?></a></h2>
                                    <p><?= $description3 ?></p>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-xs-4">
                                       <img src="<?= ImageUploader::resizeRender('/'.$image3, '130', '130'); ?>" alt="" class="pad marginleft1"/>
                                    </div>
                                    
                                    <div class="col-md-4 col-xs-4">
                                       <img src="<?= ImageUploader::resizeRender('/'.$image3, '130', '130'); ?>" alt="" class="pad marginleft2" />
                                    </div>
                                    
                                    <div class="col-md-4 col-xs-4">
                                       <img src="<?= ImageUploader::resizeRender('/'.$image3, '130', '130'); ?>" alt="" class="pad" />
                                    </div>
                                    
                                </div>
                                <div class="row" style="margin-top: 20px!important; padding-bottom: 8px!important; background-color: #f3f3f4;">
                                    <div class="col-md-4 col-xs-4">
                                       <img src="<?= ImageUploader::resizeRender('/'.$image3, '130', '130'); ?>" alt="" class="pad marginleft1" />
                                    </div>
                                    
                                    <div class="col-md-4 col-xs-4">
                                       <img src="<?= ImageUploader::resizeRender('/'.$image3, '130', '130'); ?>" alt="" class="pad marginleft2" />
                                    </div>
                                    
                                    <div class="col-md-4 col-xs-4">
                                       <img src="<?= ImageUploader::resizeRender('/'.$image3, '130', '130'); ?>" alt="" class="pad" />
                                    </div>
                                    
                                </div>
                            </div>

                        </article>
                    </div>

                    <!-- === product-item === -->
                  
                <?php }}?>
                </div><!--/row-->
       
            </div><!--/container-->
        </section>


        <section class="products padtop" style="padding-top: 20px!important;">
            <div class="container">

                <header>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title">Shop By CATEGORIES</h2>
                        </div>
                    </div>
                </header>

                <div class="row">         
                <?php if(!empty($categoryDetail)){
                foreach($categoryDetail as $value){?>
                    <div class="col-md-3 col-xs-6">
                        <article>
                            <div class="figure-block" style="background-color: #f3f3f4">
                                <div class="image">
                                     <?php if(!empty($value['image'])){?>
                                    <a href="/site/category?name=<?= $value['category_slug']?>">
                                        <img src="<?= ImageUploader::resizeRender('/'.$value['image'], '263', '220'); ?>" alt="" />
                                    </a>
                                     <?php } else {?>
                                    <a href="/site/category?name=<?= $value['category_slug']?>">
                                        <img src="/images/not-found.png" height="219" width="263" alt="" />
                                    </a>
                                    <?php }?>
                                </div>
                                <div class="text textcent">
                                    <h2 class="title h4 fon"><a href="/site/category?name=<?= $value['category_slug']?>"><?= $value['category_name']?></a></h2>
                                </div>
                            </div>
                        </article>
                    </div>                    
                <?php }}?>

                </div><!--/row-->
       
            </div><!--/container-->
        </section>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBQ28hSGPqteoC51i21YhkirkdrPu8QWMY"></script>
        <script type="text/javascript">
            function codeLatLng() {

              // var input = document.getElementById('latlng').value;
              // var latlngStr = input.split(',', 2);
              // var lat = parseFloat(latlngStr[0]);
              // var lng = parseFloat(latlngStr[1]);
              var latlng = new google.maps.LatLng(28.4594965, 28.4594965);
              var geocoder= new google.maps.Geocoder();
              geocoder.geocode({'latLng': latlng}, processRevGeocode);
            }

            function processRevGeocode(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(JSON.stringify(results[0]));
               for (var i = results[0].address_components.length - 1; i >= 0; i--) 
            {
                if(results[0].address_components[i].types[0] == 'administrative_area_level_1')
                {
                    stateName = results[0].address_components[i].long_name;
                }

                if(results[0].address_components[i].types[0] == 'postal_code')
                {
                    pincode = results[0].address_components[i].long_name;
                }

                if(results[0].address_components[i].types[0] == 'sublocality_level_1')
                {
                    locality = results[0].address_components[i].long_name;
                }
            }

            console.log(locality);
               // infowindow.setContent(results[1].formatted_address);
               // infowindow.open(map, marker);
               // console.log(JSON.stringify(results[0]));
               // displayPostcode(results[0].address_components);

            } else {
              alert('Geocoder failed due to: ' + status);
            }
        }

        // displays the resulting post code in a div
        function displayPostcode(address) {
          for (p = address.length-1; p >= 0; p--) {
            if (address[p].types.indexOf("postal_code") != -1) {
               document.getElementById('postcode').innerHTML= address[p].long_name;
            }
          }
        }
         function Button1_onclick() {
        codeLatLng();
        }

        </script>