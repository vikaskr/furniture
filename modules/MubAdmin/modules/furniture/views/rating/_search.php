<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $rating app\modules\MubAdmin\modules\furniture\ratings\RatingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rating-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($rating, 'id') ?>

    <?= $form->field($rating, 'icon_name') ?>

    <?= $form->field($rating, 'status') ?>

    <?= $form->field($rating, 'created_at') ?>

    <?= $form->field($rating, 'updated_at') ?>

    <?php // echo $form->field($rating, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
