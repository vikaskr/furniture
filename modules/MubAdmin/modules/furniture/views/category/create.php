<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\furniture\categorys\Category */

$this->title = 'Create Category';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'category' => $category,
    ]) ?>

</div>
