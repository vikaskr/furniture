<?php

namespace app\modules\MubAdmin\modules\blog\controllers;

use Yii;
use app\models\PostComment;
use app\models\PostCommentSearch;
use app\modules\MubAdmin\modules\blog\models\PostCommentProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CommentsController extends MubController
{
    public function getPrimaryModel()
    {
        return new PostComment();
    }

    public function getProcessModel()
    {
        return new PostCommentProcess();
    }

    public function getSearchModel()
    {
        return new PostCommentSearch();
    }

    public function actionApproveComment()
    {
        if (\Yii::$app->request->isAjax) 
        {
            $params = \Yii::$app->request->getBodyParams();
            $mubUserId =  \app\models\User::getMubUserId();
            $postComment = new \app\models\PostComment();
            $currentComment = $postComment::findOne($params['commentId']);
            $postOwner = $currentComment->post->mub_user_id;
            if($postOwner==$mubUserId)
            {
                $currentComment->approved_by = $mubUserId;
                $currentComment->approved_on = \Date('Y-m-d h:m:s',time());
                if($currentComment->save())
                {
                    return true;
                }
                else
                {
                    p($currentComment->getErrors());
                }
            }
        }
        return false;
    }

}
