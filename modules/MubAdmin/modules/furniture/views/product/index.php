<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\furniture\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'mub_user_id',
            'hsn_code',
            'manufacture_SKU',
            'manufacture_SKU_name:ntext',
            'product_name',
            //'product_slug',
            //'description:ntext',
            //'inner_spec_of_seat',
            //'seat_mattress',
            //'arm_rest',
            'stock_quantity',
            'base_cost',
            'sell_price',
            'mrp',
            //'time_to_ship',
            //'width',
            //'depth',
            //'height',
            //'product_group',
            // 'furniture_material',
            'fabric_type',
            //'foam_type_density',
            //'assembly',
            //'warranty',
            //'specification_general',
            //'furniture_weight',
            //'seating_height',
            //'box_content',
            //'box_count',
            'status',
            //'created_at',
            //'updated_at',
            //'del_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
