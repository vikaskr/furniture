<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\furniture\products\Product */

$this->title = 'Update Product: ' . $product->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $product->id, 'url' => ['view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'product' => $product,
        'allCategory' => $allcategory,
        'productCategories' => $productCategories,
    ]) ?>

</div>
