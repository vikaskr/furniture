<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MubUser */

$this->title = Yii::t('app', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-user-create">
<div class="col-md-10 col-md-offset-1" >
   <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'mubUser' => $mubUser,
        'mubUserContacts' => $mubUserContacts,
        'allStates' => $allStates,
        'activeStates' => $activeStates
    ]) ?>

</div>