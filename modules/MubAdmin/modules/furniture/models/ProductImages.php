<?php

namespace app\modules\MubAdmin\modules\furniture\models;
use  app\models\MubUser;

use Yii;

/**
 * This is the model class for table "product_images".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $product_id
 * @property string $thumbnail_url
 * @property string $thumbnail_path
 * @property string $full_path
 * @property string $visible
 * @property string $keyword
 * @property integer $width
 * @property integer $height
 * @property integer $display_width
 * @property integer $display_hieght
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Product $product
 */
class ProductImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'product_id', 'width', 'height', 'display_width', 'display_hieght'], 'integer'],
            [['mub_user_id','full_path'], 'required'],
            [['visible', 'type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['thumbnail_url', 'thumbnail_path', 'full_path'], 'string', 'max' => 100],
            [['keyword'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'product_id' => 'Product ID',
            'thumbnail_url' => 'Thumbnail Url',
            'thumbnail_path' => 'Thumbnail Path',
            'full_path' => 'Full Path',
            'visible' => 'Visible',
            'keyword' => 'Keyword',
            'width' => 'Width',
            'height' => 'Height',
            'display_width' => 'Display Width',
            'display_hieght' => 'Display Hieght',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
