<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use app\models\MubUser;
use app\models\State;
use yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;
/**
/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $product_name
 * @property string $product_slug
 * @property string $product_description
 * @property string $product_excerpt
 * @property string $product_type
 * @property integer $stock_quantity
 * @property integer $product_code
 * @property string $brand_name
 * @property integer $price
 * @property integer $cell_price
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property PostPingStats[] $postPingStats
 * @property MubUser $mubUser
 * @property ProductAttribute[] $productAttributes
 * @property ProductCategory[] $productCategories
 * @property ProductImages[] $productImages
 * @property ProductTags[] $productTags
 * @property UserAttribute[] $userAttributes
 */
class Product extends \app\components\Model implements CartPositionInterface
{
    use CartPositionTrait;

    public function getPrice()
    {
        return $this->mrp;
    }


    public static function tableName()
    {
        return 'product';
    }

    public function rules()
    {
        return [
            [['mrp','sell_price','product_name'],'required'],
            [['hsn_code', 'manufacture_SKU','manufacture_SKU_name','description', 'status', 'del_status','inner_spec_of_seat','seat_mattress','arm_rest','time_to_ship','product_group','furniture_material','fabric_type','foam_type_density','assembly','warranty','specification_general','furniture_weight','box_content','box_count','stock_quantity', 'mrp', 'sell_price', 'width','height','depth','base_cost','seating_height','product_slug'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'hsn_code' => 'hsn_code',
            'manufacture_SKU' => 'manufacture_SKU',
            'manufacture_SKU_name' => 'manufacture_SKU_name',
            'product_name' => 'Product Name',
            'product_slug' => 'product_slug',
            'description' => 'Description',
            'inner_spec_of_seat' => 'inner_spec_of_seat',
            'seat_mattress' => 'seat_mattress',
            'arm_rest' => 'arm_rest',
            'base_cost' => 'base_cost',
            'stock_quantity' => 'Stock Quantity',
            'base_cost' => 'Base Cost',
            'sell_price' => 'Sell Price',
            'mrp' => 'MRP',
            'time_to_ship' => 'time_to_ship',
            'width' => 'width',
            'depth' => 'depth',
            'height' => 'height',
            'product_group' => 'product_group',
            'furniture_material' => 'furniture_material',
            'fabric_type' => 'fabric_type',
            'foam_type_density' => 'foam_type_density',
            'assembly' => 'assembly',
            'warranty' => 'warranty',
            'specification_general' => 'specification_general',
            'furniture_weight' => 'furniture_weight',
            'seating_height'=> 'seating_height',
            'box_content' => 'box_content',
            'box_count' => 'box_count',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

     public function getId()
    {
        return $this->id;
    }

    public function getPostPingStats()
    {
        return $this->hasMany(PostPingStats::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttributes::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ProductCategories::className(), ['product_id' => 'id'])->where(['product_categories.del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTags()
    {
        return $this->hasMany(ProductTags::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAttributes()
    {
        return $this->hasMany(UserAttribute::className(), ['product_id' => 'id']);
    }
}
