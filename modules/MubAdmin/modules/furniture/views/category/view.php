<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\furniture\models\Category */

$this->title = $category->id;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6 col-md-offset-1">
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $category->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $category->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
</div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $category,
        'attributes' => [
            'id',
            'category_name',
            'category_slug',
            'description',
            'tag_line',
            'created_at',
            'updated_at',
            'status',
        ],
    ]) ?>
</div>
</div>
</div>
