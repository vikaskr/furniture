<?php 
namespace app\modules\MubAdmin\modules\furniture\models;

use app\components\Model;

class ReviewProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $review = new Review();
        $this->models = [
            'review' => $review, 
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getSavedRatings($rating)
    {
        $ratings = [];
        foreach ($rating as $key => $cat) 
        {
            $ratings[] = $cat->rating_id;
        }
        return $ratings;
    }

    public function getRelatedModels($model)
    {
       $review = $model;
        $this->models = [
            'review' => $review, 

        ];
        return $this->relatedModels;
    }

    public function saveReview($review)
    {
        $userId = \app\models\User::getMubUserId();
        $review->mub_user_id =  $userId;
    	return ($review->save()) ? $review->id : p($review->getErrors());
    }


    public function saveData($data)
    {
        // p($data);
    	if (isset($data['review']))
            {
            try {
                   $reviewId = $this->saveReview($data['review']);
                   if($reviewId)
                    {
                     return $reviewId;
                    }
                    p($data['review']->getErrors());
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}
