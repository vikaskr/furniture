<?php

namespace app\modules\MubAdmin\modules\furniture\controllers;

use Yii;
use app\modules\MubAdmin\modules\furniture\models\Product;
use app\modules\MubAdmin\modules\furniture\models\ProductSearch;
use app\modules\MubAdmin\modules\furniture\models\ProductProcess;
use app\modules\MubAdmin\modules\furniture\models\ProductImages;
use yii\web\UploadedFile;

use yii\helpers\FileHelper;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends MubController
{
    public function getPrimaryModel()
   {
        return new Product();
   }

   public function getProcessModel()
   {
        return new ProductProcess();
   }

   public function getSearchModel()
   {
        return new ProductSearch();
   }
    
    public function actionProductImage($userId)
   {
    if (\Yii::$app->request->isAjax) {
      $model = new ProductImages();

      $imageFile = UploadedFile::getInstance($model, 'thumbnail_url');

      $directory = \Yii::getAlias('@app/images/product/');
      if (!is_dir($directory)) {
          FileHelper::createDirectory($directory);
      }

      if ($imageFile) {
          $uid = uniqid(time(), true);
          $fileName = $uid . '.' . $imageFile->extension;
          $filePath = $directory . $fileName;
          $rid = \Yii::$app->getRequest()->getQueryParam('product');
          if ($imageFile->saveAs($filePath)) {
              $path = '/images/product/'.$fileName;
              $model->mub_user_id = $userId;
              $model->product_id = ($rid == '') ? NULL :$rid;
              $model->title = $uid . '.' . $imageFile->extension;
              $model->full_path = $path;
              $model->thumbnail_url = $path;
              if(!$model->save())
              {
                p($model->getErrors());
              }
              
              $successPath = '/images/done.png';
              return Json::encode([
                  'files' => [
                      [
                          'name' => $fileName,
                          'size' => $imageFile->size,
                          'url' => $successPath,
                          'thumbnailUrl' => $successPath,
                          'deleteUrl' => 'image-delete?name=' . $fileName,
                          'deleteType' => 'POST',
                      ],
                  ],
              ]);
          }
      }
      return '';
    }else
    {
      return 'go away';
    }
  }

  public function actionImageDelete($name)
  {
     if (\Yii::$app->request->isAjax) {
      $stateId = \Yii::$app->request->getQueryParam('state');
      $directory = \Yii::getAlias('@app/images/product') . DIRECTORY_SEPARATOR;
      if (is_file($directory.$name)) 
      {
          if(unlink($directory . $name))
          {
            $productImages = new \app\modules\MubAdmin\modules\item\models\ProductImages();
            $success = $productImages::deleteAll(['title' => $name]);
            if($success)
            {
              return 'Image Deleted Successfully'; 
            }
          }else
          {
            return 'There was a problem deleting the Image';
          }
      }
      else
      {
        $productImages = new \app\modules\MubAdmin\modules\item\models\ProductImages();
        $success = $productImages::deleteAll(['title' => $name]);
        if($success)
        {
          return 'Image Deleted from Records'; 
        }
      }
      
  }else
  {
    return 'Go Awayyyy!';
  }
}
}