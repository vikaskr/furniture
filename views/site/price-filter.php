<?php 
use \app\helpers\ImageUploader;
use yii\data\Pagination;
use yii\widgets\LinkPager;
$productImg = new \app\modules\MubAdmin\modules\furniture\models\ProductImages();
?>
 <section class="products" style="padding-top: 40px!important;">

            <div class="container">

                <div class="row">

                    <!-- === product-items === -->

                    <div class="col-md-12 col-xs-12">

                         <div class="row" style="padding-bottom: 20px; margin-top: -65px;">
                           <?php 
                           foreach ($sortPrice as $value) { 
                            $relPro= $value['id'];
                            $relImg = $productImg::find()->where(['del_status' => '0','product_id' => $relPro])->one();
                            ?>
                           <div class="col-md-6 col-xs-6">
                                <article>
                                    <div class="figure-grid">
                                        <div class="image">
                                            <a href="/site/product?name=<?= $value['product_slug']?>">
                                                <?php if(!empty($relImg->thumbnail_url)){?>
                                                <img src="<?= ImageUploader::resizeRender($relImg->thumbnail_url, '533', '380'); ?>" alt="" style="height: 360px;" />
                                                <?php }else{?>
                                                <img src="/images/not-found.png" alt="" style="height: 360px;" />
                                                <?php }?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="figure-grid">
                                         <div class="text">
                                            <h2 class="title h5"><a href="/site/product?name=<?= $value['product_slug']?>"><?= $value['product_name']?></a></h2> <button class="pull-right btn btn-small" style="background-color: #cc9600;"><a href="/site/product?name=<?= $value['product_slug']?>" style="color:#fff;">View Detail</a></button>
                                            <sub class="fontsize">Retail Price</sub><br/>
                                          <!--   <sup class="fontsize">Offer Price</sup> -->
                                           <!--  <sub>₹ <?= $value['mrp']?>,</sub> -->
                                            <sup>₹ <?= $value['mrp']?></sup><br/>
                                            <br>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <?php }?>
                        </div>
                    </div> <!--/product items-->
                </div><!--/row-->
            </div><!--/container-->
            <div class="row"><div class="col-md-2"></div><div class="col-md-8"><center><?= LinkPager::widget([
                             'pagination' => $pages,
                             ]);
                            ?></center></div>
                </div>
        </section>