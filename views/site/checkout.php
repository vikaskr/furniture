<?php 

use \app\modules\MubAdmin\modules\furniture\models\ProductImages;
use yz\shoppingcart\ShoppingCart;
use \app\helpers\ImageUploader;

$cart = new ShoppingCart();
$cartItems = $cart->getPositions();

?>





   <section class="main-header" style="background-image:url(/images/gallery-2.jpg)">
            <header>
                <div class="container text-center">
                    <h2 class="h2 title">Checkout</h2>
                    <ol class="breadcrumb breadcrumb-inverted">
                        <li><a href="index.html"><span class="icon icon-home"></span></a></li>
                        <li><a class="active" href="checkout-1.html">Cart items</a></li>
                        <li><a href="checkout-2.html">Delivery</a></li>
                        <li><a href="checkout-3.html">Payment</a></li>
                        <li><a href="checkout-4.html">Receipt</a></li>
                    </ol>
                </div>
            </header>
        </section>

        <!-- ========================  Checkout ======================== -->
        <?php if(!empty($cartItems)){?>
        <section class="checkout">

            <div class="container">


                <!-- ========================  Cart wrapper ======================== -->

                <div class="cart-wrapper">
                    <!--cart header -->

                    <div class="cart-block cart-block-header clearfix">
                        <div>
                            <span>Product Image</span>
                        </div>
                        <div>
                            <span>Product Name</span>
                        </div>
                        <div>
                            <span>Quantity</span>
                        </div>
                        <div class="text-right">
                            <span>Price</span>
                        </div>
                    </div>

                    <!--cart items-->
                 <?php foreach($cartItems as $value){
                    $productId = $value['id'];

                    $productImg = new \app\modules\MubAdmin\modules\furniture\models\ProductImages();
                    $proImg = ProductImages::find()->where(['del_status' => '0','product_id' => $productId])->one(); ?>
                    <div class="clearfix">
                        
                        <div class="cart-block cart-block-item clearfix" style="border-bottom: 1px solid #cacaca; padding-top: 1em; padding-bottom: 2em;">
                            <div class="image">
                                <img src="<?= ImageUploader::resizeRender($proImg['thumbnail_url'], '200', '150'); ?>" alt="" style="padding-top: 10px; padding-bottom: 10px;" />
                            </div>
                            <div class="title">
                                <div class="h4"><a href="/site/product?name=<?= $value['product_slug'];?>"><?=$value['product_name'];?></a></div>
                            </div>
                            <div class="">
                                <span><i class="fa fa-minus decrementcart" id="dec_<?=$productId;?>"></i></span>
                                <span id="mgQuant_<?= $productId;?>" style="background: grey; color: #fff; padding: 8px;"><?= $value->getQuantity();?></span>
                                <span><i class="fa fa-plus incrementcart" id="inc_<?=$productId;?>"></i></span>
                             <?php
                            $alreadyInCart = $cart->getPositionById($productId);
                            ?>

                                 <br/><br/> <a class="btn-updateaddcart cusbutton" id="add-cart_<?= $productId;?>" style="cursor: pointer; background: orange; color: #fff;  padding: 5px;margin-top: 5px;">Update</a>
                            </div>
                            <div class="price">
                                <span class="final h3">₹ <?=($value['mrp'])*($value->getQuantity());?></span>
                            </div>
                            <!--delete-this-item-->
                            <a class="btn-remove-item" id="remove_item_<?= $value['id'];?>" style="margin-top: 4px; font-size: 20px; color: red;"><span class="icon icon-cross icon-delete"></span></a>
                        </div>

                    </div>
                    <?php }?>
     
                    <div class="clearfix">
                        <div class="cart-block cart-block-footer cart-block-footer-price clearfix">
                            <div>
                                <span class="checkbox" style="display: none;">
                                    <input type="checkbox" id="couponCodeID">
                                    <label for="couponCodeID"></label>
                                    <input type="text" class="form-control form-coupon" value="" placeholder="Enter your coupon code" />
                                </span>
                            </div>
                            <div>
                                <h3 class="value"><div id="display-price">Total Amount: ₹ <?= $cart->getCost()?></div></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ========================  Cart navigation ======================== -->

                <div class="clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="/" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> Shop more</a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="/site/payments" class="btn btn-main"><span class="icon icon-cart"></span> Proceed to delivery</a>
                        </div>
                    </div>
                </div>

            </div> <!--/container-->

        </section>
        <?php } else{?>
        <h1 style="text-align: center; padding: 3em;">Your Cart is Empty</h1>
        
        <?php }?>
