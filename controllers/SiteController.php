<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ClientSignup;
use app\models\ContactForm;
use yz\shoppingcart\ShoppingCart;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use \app\modules\MubAdmin\modules\furniture\models\Product;
use \app\modules\MubAdmin\modules\furniture\models\Category;
use \app\modules\MubAdmin\modules\furniture\models\ProductCategories;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
       $category = new \app\modules\MubAdmin\modules\furniture\models\Category();

       $this->view->params['page'] = 'home';
       return $this->render('index',['product' => $product,'category' => $category]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    

   public function actionClientLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();

            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('signin', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }
    
    public function actionClientRegister()
    {
        // p(Yii::$app->request->getBodyParams());
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            if ($model->load(Yii::$app->request->post())) 
            {     
                if ($user = $model->signup()) 
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                    $mubUserId = \app\models\User::getMubUserId();
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;
                    \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setCc('info@osmstays.com')
                        ->setSubject('Your Profile Created')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }
                else
                {
                   $model->addError('username','This Username Already Exists in Database');
                   return $this->renderAjax('signup',['model' => $model]);
                }
            }
            return $model->getErrors();
        }
    }

    public function actionClientRegisterValidate()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            $model->load(Yii::$app->request->post());
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionSignup()
    {
        return $this->renderAjax('signup');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSuccess()
    {
        return $this->render('success');
    }

    public function actionCategory($name,$price= "")
    {
         if (\Yii::$app->request->isAjax) 
         {
           $params = \Yii::$app->request->getQueryParams();
            $catname = $params['name'];  
            if($params['price'] == 'plth')
            {
               $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
               $cat = Category::find()->where(['del_status' => '0','category_slug' => $catname])->one();
               $catId = $cat->id;
               $sortPro = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->all();
               $countQuery = count($sortPro); 
               $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 20]);
               $sortPrice = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->orderBy(['mrp'=>SORT_ASC])->limit($pages->limit)->offset($pages->offset)->all();

               return $this->renderAjax('price-filter',['sortPrice' => $sortPrice,'pages' => $pages]);
            }

            elseif($params['price'] == 'phtl')
            {
               $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
               $cat = Category::find()->where(['del_status' => '0','category_slug' => $catname])->one();
               $catId = $cat->id;
               $sortPro = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->all();
               $countQuery = count($sortPro); 
               $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 20]);
               $sortPrice = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->orderBy(['mrp'=>SORT_DESC])->limit($pages->limit)->offset($pages->offset)->all();

               return $this->renderAjax('price-filter',['sortPrice' => $sortPrice,'pages' => $pages]);
            }
            else{
                 return $this->render('category');
            }
         }
         else
         {
           $params = Yii::$app->request->getQueryParams();
           $urlData = $params['name'];
            if(!empty($params['price']))
            { 
                if($params['price'] == 'plth')
                {
                   $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
                   $cat = Category::find()->where(['del_status' => '0','category_slug' => $urlData])->one();
                   $catId = $cat->id;
                   $catName = $cat->category_slug;
                   $sortPro = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->all();
                   $countQuery = count($sortPro); 
                   $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 20]);
                   $productData = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->orderBy(['mrp'=>SORT_ASC])->limit($pages->limit)->offset($pages->offset)->all();

                   return $this->render('category',['pages' => $pages, 'productData' => $productData,'countQuery' => $countQuery,'catName' => $catName]);
                }

                elseif($params['price'] == 'phtl')
                {
                   $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
                   $cat = Category::find()->where(['del_status' => '0','category_slug' => $urlData])->one();
                   $catId = $cat->id;
                   $catName = $cat->category_slug;
                   $sortPro = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->all();
                   $countQuery = count($sortPro); 
                   $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 20]);
                   $productData = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->orderBy(['mrp'=>SORT_DESC])->limit($pages->limit)->offset($pages->offset)->all();

                   return $this->render('category',['pages' => $pages, 'productData' => $productData,'countQuery' => $countQuery,'catName' => $catName]);
                }
            }
            elseif($params['name'])
            {
               $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
               $cat = Category::find()->where(['del_status' => '0','category_slug' => $urlData])->one();
               $catId = $cat->id;
               $catName = $cat->category_slug;
               $sortPro = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->all();
               $countQuery = count($sortPro); 
               $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 20]);
               $productData = $product::find()->innerJoin('product_categories','product.id=product_categories.product_id')->where(['product.del_status' => '0','category_id' => $catId])->orderBy(['id'=>SORT_DESC])->limit($pages->limit)->offset($pages->offset)->all();
               return $this->render('category',['productData' => $productData,'countQuery' => $countQuery,'pages' => $pages, 'catName' => $catName]);
            }
        }
    }

    public function actionAllcategory()
    {
        $category = new \app\modules\MubAdmin\modules\furniture\models\Category();
        return $this->render('allcategory',['category' => $category]);
    }

    public function actionProduct($name)
    {
       $singleProduct = new \app\modules\MubAdmin\modules\furniture\models\Product();
       $products = $singleProduct::find()->where(['product_slug' => $name,'del_status' => '0'])->one();

        return $this->render('product',[
            'products' => $products]);
    }

    public function actionAllproduct()
    {
       $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
       $category = new \app\modules\MubAdmin\modules\furniture\models\Category();
       return $this->render('allproduct',['product' => $product,'category' => $category]);
    }


    public function actionAddComment()
    {
        if (\Yii::$app->request->isAjax) 
        {
            $params = \Yii::$app->request->getBodyParams();
            if(!empty($params['PostComment']['url'])){
                $postUrl = $params['PostComment']['url'];
                $postsModel = new \app\models\Post();
                $post = $postsModel::find()->where(['del_status'=>'0','url' => $postUrl])->one();
                $postComments = new \app\models\PostComment();
                $postComments->load(\Yii::$app->request->getBodyParams());
                $postComments->post_id = $post->id;
                if($postComments->save())
                {
                   return $postComments->id; 
                }
            }
            return false;
        }
        else
        {
            throw new \yii\web\HttpException(400, 'This path is wrongly accessed');
        }
    }
    public function actionForgetpass()
    {
        $this->layout =false;
        if(Yii::$app->request->post())
        {
            $postData = \Yii::$app->request->post();
            $mubUserModel = new \app\models\MubUser();
            $mubUserContactModel = new \app\models\MubUserContact();
            $mubUserContact = $mubUserContactModel::find()->where(['email' => $postData['ClientSignup']['email']])->one();
            if(!empty($mubUserContact))
            {  
                $mubUser = $mubUserModel::find()->where(['id' => $mubUserContact->mub_user_id, 'del_status' => '0'])->one();
                $userEmail = $mubUserContact['email'];
                \Yii::$app->mailer->compose('forget',['mubUserContact' => $mubUserContact,'mubUser' => $mubUser])
                    ->setFrom('info@mayaexpress.in')
                    ->setTo($mubUserContact->email)
                    ->setCc('info@mayaexpress.in')
                    ->setSubject('Your Mayaexpress Password')->setBcc('praveen@makeubig.com','MakeUBIG ADMIN')->send();
                    return 'mailsent';
            }
        }
        return $this->render('forgetpass');
    }
    public function actionAddtocart($id)
    {   
        $cart = new ShoppingCart();
        $this->layout = false;
        $model = Product::findOne($id);
        if ($model){
            $cart->put($model, 1);
            return $this->render('showcart', [
                'cart' => $cart,
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionClearCart($id=NULL)
    {
         $cart = new ShoppingCart();
         $this->layout = false;
         if($id == NULL)
         {
            $cart->removeAll();
         }
         else
         {
            $cart->removeById($id);  
         }
         return $this->render('showcart', [
            'cart' => $cart]);
    }

    public function actionShowcart()
    {
        $this->layout = false;
        $cart = new ShoppingCart();
        $items = $cart->getPositions();
        return $this->render('showcart', [
            'items' => $items]);  
    }

    public function actionUpdateCartItems($id,$quantity)
    {  
        if($quantity >= 0)
        {
            $cart = new ShoppingCart();
            $this->layout = false;
            $model = Product::findOne($id);
            if($model)
            {
                $cart->update($model, $quantity);
            }
            $total = $cart->getCost(); 
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data['total'] = $total;
            return $data;
        } 
    }

    public function actionCheckout()
    {
        $this->view->params['page'] = 'checkout';
        return $this->render('checkout');
    }

    public function actionPayments()
    {
        $this->view->params['page'] = 'payments';
        return $this->render('payments');
    }

    public function actionCartcount()
    {
        $this->layout = false;
        $this->view->params['page']='cartcount';
        $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        return $this->render('cartcount', ['count' => $count]);
    }

    public function actionPriceFilter()
    {
        return $this->render('price-filter');
    }
}

