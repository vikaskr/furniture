<?php

namespace app\modules\MubAdmin\modules\furniture;

class Furniture extends \app\modules\MubAdmin\MubAdmin
{   
    public $controllerNamespace = 'app\modules\MubAdmin\modules\furniture\controllers';
    
    public $defaultRoute = 'product';
    
    public function init()
    {
        parent::init();
    }
}