<?php
use \app\models\Booking;

$booking = new Booking;
$order = $booking::find()->where(['del_status' => '0'])->all();
?>
<style type="text/css">
	th{
		font-size: 17px;
		padding: 15px;
	}
	td {
		padding: 15px;
		font-size: 13px;
	}
</style>
<script type="text/javascript">
	function showDiv() {
   document.getElementById('welcomeDiv').style.display = "block";
}
</script><br/>
<h1 style="text-decoration: underline;">Welcome to Dashboard</h1><br/>
<input type="button" class="btn btn-success" name="answer" value="Show Order History" onclick="showDiv()" />
<div class="container" id="welcomeDiv"  style="display:none;">
<h2>Furniture Minds Order History</h2><br/>
	<div class="row">
		<table border="2" width="100%">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Mobile</th>
				<th>Product</th>
				<th>Qnt.</th>
				<th>Price</th>
				<th>Address</th>
				<th>City</th>
				<th>Pincode</th>
				<th>Memo</th>
			</tr>
			<?php 
			foreach($order as $value) {
			?>
			<tr>
				<td><?= $value['id'];?></td>
				<td><?= $value['first_name'].' '.$value['first_name'];?></td>
				<td><?= $value['mobile'];?></td>
				<td><?= $value['product'];?></td>
				<td><?= $value['quantity'];?></td>
				<td><?= $value['amount'];?></td>
				<td><?= $value['delivery_address'];?></td>
				<td><?= $value['city'];?></td>
				<td><?= $value['pin_code'];?></td>
				<td><button class="btn btn-success">PDF</button></td>
			</tr>
			<?php }?>
		</table>
	</div>
</div>