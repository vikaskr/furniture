<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/bootstrap.css',
        '/css/animate.css',
        '/css/font-awesome.css',
        '/css/furniture-icons.css',
        '/css/linear-icons.css',
        '/css/magnific-popup.css',
        '/css/owl.carousel.css',
        '/css/ion-range-slider.css',
        '/css/theme.css',
        
    ];
    public $js = [
        '/js/custom_mub_frontend.js',
        '/js/jquery.magnific-popup.js',
        '/js/jquery.owl.carousel.js',
        '/js/jquery.ion.rangeSlider.js',
        '/js/jquery.isotope.pkgd.js',
        '/js/main.js',
         
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesomeAsset'        
    ];
}
