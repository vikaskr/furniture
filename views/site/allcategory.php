<?php 
$category = new \app\modules\MubAdmin\modules\furniture\models\Category();
$categoryDetails = $category::find()->where(['del_status' => '0'])->limit(12)->all();
?>
       
            <!-- === header === -->

            <header class="hidden">
                <h2>Product categories</h2>
            </header>

            <div class="container">

                <div class="owl-icons">

                    <!-- === icon item === -->
                     <?php foreach($categoryDetails as $value){?>
                    <a href="/site/category?name=<?= $value['category_slug']?>">
                        <figure>
                            <i class="f-icon <?= $value['icon_name']?>"></i>
                            <figcaption style="text-align: center;"><?= $value['category_name']?></figcaption>
                        </figure>
                    </a>
                    <?php }?>
                </div> <!--/owl-icons-->
            </div> <!--/container-->
