<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "furniture".
 *
 * @property int $id
 * @property int $mub_user_id
 * @property string $hsn_code
 * @property string $manufacture_SKU
 * @property string $manufacture_SKU_name
 * @property string $product_name
 * @property string $product_slug
 * @property string $description
 * @property string $inner_spec_of_seat
 * @property string $Seat Mattress
 * @property string $arm_rest
 * @property int $stock_quantity
 * @property int $base_cost
 * @property int $sell_price
 * @property int $mrp
 * @property string $time_to_ship
 * @property string $width
 * @property string $depth
 * @property string $height
 * @property string $product_group
 * @property string $furniture_material
 * @property string $fabric_type
 * @property string $foam_type_density
 * @property string $Assembly
 * @property string $warranty
 * @property string $specification_general
 * @property string $furniture_weight
 * @property string $seating_height
 * @property string $box_content
 * @property string $box_count
 * @property string $tag
 * @property string $category
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property MubUser $mubUser
 */
class Furniture extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'furniture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'product_slug', 'mrp'], 'required'],
            // [['stock_quantity', 'base_cost', 'sell_price', 'mrp'], 'integer'],
            [['manufacture_SKU_name', 'description', 'status', 'del_status','inner_spec_of_seat'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['hsn_code', 'manufacture_SKU', 'product_name', 'product_slug', 'seat_mattress', 'arm_rest', 'time_to_ship', 'width', 'depth', 'height', 'product_group', 'fabric_type', 'foam_type_density', 'assembly', 'warranty', 'furniture_weight', 'seating_height', 'box_content', 'box_count', 'tag', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hsn_code' => 'Hsn Code',
            'manufacture_SKU' => 'Manufacture  Sku',
            'manufacture_SKU_name' => 'Manufacture  Sku Name',
            'product_name' => 'Product Name',
            'product_slug' => 'Product Slug',
            'description' => 'Description',
            'inner_spec_of_seat' => 'Inner Spec Of Seat',
            'seat_mattress' => 'Seat  Mattress',
            'arm_rest' => 'Arm Rest',
            'stock_quantity' => 'Stock Quantity',
            'base_cost' => 'Base Cost',
            'sell_price' => 'Sell Price',
            'mrp' => 'Mrp',
            'time_to_ship' => 'Time To Ship',
            'width' => 'Width',
            'depth' => 'Depth',
            'height' => 'Height',
            'product_group' => 'Product Group',
            'furniture_material' => 'Furniture Material',
            'fabric_type' => 'Fabric Type',
            'foam_type_density' => 'Foam Type Density',
            'assembly' => 'Assembly',
            'warranty' => 'Warranty',
            'specification_general' => 'Specification General',
            'furniture_weight' => 'Furniture Weight',
            'seating_height' => 'Seating Height',
            'box_content' => 'Box Content',
            'box_count' => 'Box Count',
            'tag' => 'Tag',
            'category' => 'Category',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
