<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $post app\models\Post */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Post',
]) . $post->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $post->id, 'url' => ['view', 'id' => $post->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'post' => $post,
        'postDetail' => $postDetail,
        'postImages' => $postImages,
        'postCategory' => $postCategory,
        'allCategories' => $allCategories
    ]) ?>

</div>