<?php 
namespace app\modules\MubAdmin\modules\furniture\models;

use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class RatingProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $rating = new Rating();
        $this->models = [
            'rating' => $rating
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $rating = $model;
        $this->relatedModels = [
            'rating' => $rating
        ];
        return $this->relatedModels;
    }


    public function saveRating($rating)
    {
        return ($rating->save()) ? $rating->id : p($rating->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['rating']))
        {
        try {
        	$catId = $this->saveRating($data['rating']);
        	return ($catId) ? $catId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
