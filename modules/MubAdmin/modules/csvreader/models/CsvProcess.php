<?php 

namespace app\modules\MubAdmin\modules\csvreader\models;
// use app\modules\MubAdmin\modules\csvreader\models\Furniture;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use \app\modules\MubAdmin\modules\furniture\models\Product;

use Yii;


class CsvProcess extends Model
{
	  public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $this->models = [''];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [];
        return $this->relatedModels;
    }

    public function saveProductCategory($productId,$tags)
    {
      
      $selectedCategories = $tags;

      $productCategories = new \app\modules\MubAdmin\modules\furniture\models\ProductCategories();
      $mubUserId = \app\models\User::getMubUserId();
      $attribs = ['product_id','category_id','mub_user_id','created_at'];
      $recordSet = [];
      if (is_array($selectedCategories) || is_object($selectedCategories))
      {
      foreach ($selectedCategories as $categoryId) {
          $recordSet[] = [$productId,$categoryId,$mubUserId,date('Y-m-d h:m:s',time())];
      }
      }
      $catCount = count($selectedCategories);

      $productCategories->deleteAll(['product_id' => $productId]);
      $inserted = Yii::$app->db->createCommand()->batchInsert($productCategories->tableName(), $attribs, $recordSet)->execute();
    }


    public function saveFurniture($data)
    {
      
      $furnitureModel = new \app\modules\MubAdmin\modules\furniture\models\Product();
      
      if($furnitureModel->load($data) && $furnitureModel->validate())
        { 
        $slug = $data['Product']['product_slug'];
        $exists = $furnitureModel::find()->where(['product_slug' => $slug])->one();
        if($exists)
        { 
          $response = $furnitureModel->updateAll($data['Product'],['product.id' => $exists->id]);
          return $exists->id;
        }
        else
        { 
          if(!$furnitureModel->save())
          {   
              p($furnitureModel->getErrors());
          }
          $productId = $furnitureModel->id;
          $tags = $data['tags'];
          $this->saveProductCategory($productId,$tags);
          return $productId;
        }
      }
      else
      { 
        p($furnitureModel->getErrors());
      }
    }

    public function saveData($csvFile)
    {
      $tags = new \app\modules\MubAdmin\modules\furniture\models\Category();

      $categoryDb = $tags::find()->where(['del_status' => '0'])->all();

      foreach ($categoryDb as $value) 
      {
        $category_slug = $value['category_slug'];
      }

      ini_set('max_execution_time', 200000);
      ini_set('memory_limit', '-1');
        if(!empty($csvFile))
        {
           foreach ($csvFile as $rowKey => $row) 
           {
             $dataSet = explode(';',$row);
             if($rowKey == 0)
             {
             }
              else
              {

               $tagsColumn = (isset($dataSet[27]) && ($dataSet[27] !== ''))? $dataSet[27] : 'NA';
               $productTags = (explode(",",$tagsColumn));

               if(is_array($productTags))
               {
                $allTagIds = [];
                foreach($productTags as $tag)
                {
                  $categorySlug = \app\helpers\StringHelper::generateSlug($tag); 
                  $category_id = $tags::find()->where(['category_slug' => $categorySlug])->one();
                  if(isset($category_id->id))
                  {
                    //entry made id the category found in DB
                    $allTagIds[] = $category_id->id;
                  }else{
                    //throw an exception that category not found
                    throw new \yii\base\Exception( "Wrong category name entered" );
                  }
                }
                
               }

               else
               {
                $categoryId = $category->id;
               }
              
               if($dataSet[0] != '')
               {
                 $desc = (isset($dataSet[4]) && ($dataSet[4] !== ''))? $dataSet[4] : 'NA';
                 $descr = str_replace('"', ' ', $desc);
                 $descri = str_replace('\n', ' ', $descr);
                 $description = str_replace('\r', ' ', $descri);
                 $spec = (isset($dataSet[22]) && ($dataSet[22] !== ''))? $dataSet[22] : 'NA';
                 $speci = str_replace('"', ' ', $spec);
                 $specif = str_replace('\n', ' ', $speci);
                 $specification = str_replace('\r', ' ', $specif);
                 $slugMask = $dataSet[3];
                 $userId = \app\models\User::getMubUserId();

                 $data['Product']['mub_user_id'] =  $userId;
                 $data['Product']['hsn_code'] = (isset($dataSet[0]) && ($dataSet[0] !== ''))? $dataSet[0] : 'NA';
                 $data['Product']['manufacture_SKU'] = (isset($dataSet[1]) && ($dataSet[1] !== ''))? $dataSet[1] : 'NA';
                 $data['Product']['manufacture_SKU_name'] = (isset($dataSet[2]) && ($dataSet[2] !== ''))? $dataSet[2] : 'NA';
                 $data['Product']['product_name'] = (isset($dataSet[3]) && ($dataSet[3] !== ''))? $dataSet[3] : 'NA';
                 $data['Product']['product_slug'] = \app\helpers\StringHelper::generateSlug($slugMask);
                 $data['Product']['description'] = $description;
                 $data['Product']['inner_spec_of_seat'] = (isset($dataSet[5]) && ($dataSet[5] !== ''))? $dataSet[5] : 'NA';
                 $data['Product']['seat_mattress'] = (isset($dataSet[6]) && ($dataSet[6] !== ''))? $dataSet[6] : 'NA';
                 $data['Product']['arm_rest'] = (isset($dataSet[7]) && ($dataSet[7] !== ''))? $dataSet[7] : 'NA';
                 $data['Product']['stock_quantity'] = (isset($dataSet[8]) && ($dataSet[8] !== ''))? $dataSet[8] : 'NA';
                 $data['Product']['base_cost'] = (isset($dataSet[9]) && ($dataSet[9] !== ''))? $dataSet[9] : 'NA';
                 $data['Product']['sell_price'] = (isset($dataSet[10]) && ($dataSet[10] !== ''))? $dataSet[10] : 'NA';
                 $data['Product']['mrp'] = (isset($dataSet[11]) && ($dataSet[11] !== ''))? $dataSet[11] : 'NA';
                 $data['Product']['time_to_ship'] = (isset($dataSet[12]) && ($dataSet[12] !== ''))? $dataSet[12] : 'NA';
                 $data['Product']['width'] = (isset($dataSet[13]) && ($dataSet[13] !== ''))? $dataSet[13] : 'NA';
                 $data['Product']['depth'] = (isset($dataSet[14]) && ($dataSet[14] !== ''))? $dataSet[14] : 'NA';
                 $data['Product']['height'] = (isset($dataSet[15]) && ($dataSet[15] !== ''))? $dataSet[15] : 'NA';
                 $data['Product']['product_group'] = (isset($dataSet[16]) && ($dataSet[16] !== ''))? $dataSet[16] : 'NA';
                 $data['Product']['furniture_material'] = (isset($dataSet[17]) && ($dataSet[17] !== ''))? $dataSet[17] : 'NA';
                 $data['Product']['fabric_type'] = (isset($dataSet[18]) && ($dataSet[18] !== ''))? $dataSet[18] : 'NA';
                 $data['Product']['foam_type_density'] = (isset($dataSet[19]) && ($dataSet[19] !== ''))? $dataSet[19] : 'NA';
                 $data['Product']['assembly'] = (isset($dataSet[20]) && ($dataSet[20] !== ''))? $dataSet[20] : 'NA';
                 $data['Product']['warranty'] = (isset($dataSet[21]) && ($dataSet[21] !== ''))? $dataSet[21] : 'NA';
                 $data['Product']['specification_general'] = $specification;
                 $data['Product']['furniture_weight'] = (isset($dataSet[23]) && ($dataSet[23] !== ''))? $dataSet[23] : 'NA';
                 $data['Product']['seating_height'] = (isset($dataSet[24]) && ($dataSet[24] !== ''))? $dataSet[24] : 'NA';
                 $data['Product']['box_content'] = (isset($dataSet[25]) && ($dataSet[25] !== ''))? $dataSet[25] : 'NA';
                 $data['Product']['box_count'] = (isset($dataSet[26]) && ($dataSet[26] !== ''))? $dataSet[26] : 'NA';
                 $data['tags'] = $allTagIds;
                 if($data['Product']['product_name'] != 'NA')
                 {
                   $FurnitureId = $this->saveFurniture($data);
                 }
                }
             }
           }
        }
        return true;
    }
}