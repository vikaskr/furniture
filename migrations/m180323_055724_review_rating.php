<?php

namespace app\migrations;
use app\commands\Migration;

/**
 * Class m180323_055724_review_rating
 */
class m180323_055724_review_rating extends Migration
{
      public function getTableName()
    {
        return 'review_rating';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'review_id' => ['product_review','id'],
            'rating_id' => ['rating','id']
        ];
    }

    public function getKeyFields()
    {
        return [];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'review_id' => $this->integer()->notNull(),
            'rating_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
