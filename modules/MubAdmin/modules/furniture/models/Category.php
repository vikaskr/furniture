<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use app\models\MubUser;
use app\models\State;
use app\models\MubCategory;
use yii\web\UploadedFile;



/**
 * This is the model class for table "product_category".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $category_id
 * @property string $category_name
 * @property string $category_slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubCategory $category
 * @property Product $product
 */
class Category extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['created_at', 'updated_at','icon_name'], 'safe'],
            [['category_name'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png','maxFiles' => 10],
            [['del_status','status'], 'string'],
            [['category_name', 'category_slug','description','tag_line'], 'string', 'max' => 255],
        
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'category_slug' => 'Category Slug',
            'description' => 'Description',
            'tag_line' => 'Tag Line',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
    }
