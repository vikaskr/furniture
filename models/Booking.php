<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile
 * @property string $landline
 * @property string $order_id
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $pin_code
 * @property string $delivery_address
 * @property string $product
 * @property integer $amount
 * @property string $currency
 * @property string $txn_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_address'],'required'],
            [['mub_user_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'email', 'mobile'], 'string', 'max' => 50],
            [['city', 'state', 'country', 'pin_code', 'delivery_address','quantity', 'product', 'txn_id'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'landline' => 'Landline',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
             'quantity' => 'quantity',
            'pin_code' => 'Pin Code',
            'delivery_address' => 'Delivery Address',
            'product' => 'product',
            'amount' => 'Amount',
            'txn_id' => 'Txn ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
