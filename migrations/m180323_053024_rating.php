<?php

namespace app\migrations;
use app\commands\Migration;

class m180323_053024_rating extends Migration
{
    public function getTableName()
    {
        return 'rating';
    }
    public function getForeignKeyFields()
    {
        return [];
    }

    public function getKeyFields()
    {
        return [
            'status' => 'status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'icon_name' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
    
}
