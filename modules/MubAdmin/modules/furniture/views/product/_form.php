<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileupload\FileUploadUI;
use app\helpers\ImageUploader;
use app\modules\MubAdmin\modules\furniture\models\ProductImages;
use yii\web\JsExpression;
use dosamigos\tinymce\TinyMce;
use wbraganca\dynamicform\DynamicFormWidget;

$productImages = new ProductImages();
$images = $productImages::find()->where(['product_id' => $product->id,'del_status' => '0'])->all();
$mubUserId = \app\models\User::getMubUserId();
/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\furniture\products\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="product-form">
<div class="row">
    <div class="col-md-10 col-md-offset-1">


    <h5><b>Product Images</b></h5>
    <?= FileUploadUI::widget([
    'model' => $productImages,
    'attribute' => 'thumbnail_url',
    'url' => ['product/product-image', 'userId' => $mubUserId, 'product' => $product->id],
    'gallery' => false,
    'fieldOptions' => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 5000000,
        'load' => true
    ],
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
    ],
]); ?>
</div>
</div>

    <?php $form = ActiveForm::begin(); ?>
   <?php if(!empty($images)){?>
    <?php  foreach($images as $key=>$image){

        ?>
    <div id="uploaded<?php echo $key;?>" style="display: inline; margin-right: 10px;">
    <?php echo Html::img(ImageUploader::resizeRender($image->thumbnail_url, '100', '100'),['height'=>100,'width'=>100]);
    echo Html::a(
        Yii::t('app', 'x'),
        '#',
        ["class" => "btn btn-danger del-image","id" => $image->title]);
    ?>
     </div>
     <?php }}?><br/><br/>

  <div class="row">
    <div class="col-md-5 col-md-offset-1">
    <?= $form->field($product, 'hsn_code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5">
        <?= $form->field($product, 'manufacture_SKU')->textInput();?>
    </div>
  </div>

 <div class="row">
    <div class="col-md-5 col-md-offset-1">
    <?= $form->field($product, 'manufacture_SKU_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5">
        <?= $form->field($product, 'product_name')->textInput();?>
    </div>
  </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <?= $form->field($product, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'en_GB',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
        ]);?>
        </div>
    </div>


    <div class="row">
      <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'inner_spec_of_seat')->textInput(['maxlength' => true]);?>
       </div>
       <div class="col-md-5">
        <?= $form->field($product, 'seat_mattress')->textInput();?>
       </div>
     </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'arm_rest')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'stock_quantity')->textInput(['maxlength' => true]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'base_cost')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'sell_price')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'mrp')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'time_to_ship')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'width')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'depth')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'height')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'product_group')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'furniture_material')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'fabric_type')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'foam_type_density')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'assembly')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'warranty')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'specification_general')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'furniture_weight')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'seating_height')->textInput();?>
       </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'box_content')->textInput();?>
        </div>
        <div class="col-md-5">
        <?= $form->field($product, 'box_count')->textInput();?>
       </div>
    </div>

   <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($product, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']);?>
      </div>
   </div>

   <div class="row">
    <div class=" col-md-5 col-md-offset-1">
    <?php echo $form->field($productCategories, 'category_id')->checkboxList($allCategory)->label('Select Product Tags'); ?>
   </div>
    </div>
    
    <div class="row">
    <div class="col-md-5 col-md-offset-1">
    <div class="form-group">
        <?= Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);?>
    </div>
    </div>
    </div><br/>
    <?php ActiveForm::end(); ?>

</div>
