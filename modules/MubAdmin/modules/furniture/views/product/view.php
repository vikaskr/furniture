<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\furniture\models\Product */

$this->title = $product->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $product->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $product->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $product,
        'attributes' => [
            'id',
            'mub_user_id',
            'hsn_code',
            'manufacture_SKU',
            'manufacture_SKU_name:ntext',
            'product_name',
            'product_slug',
            'description:ntext',
            'inner_spec_of_seat',
            'seat_mattress',
            'arm_rest',
            'stock_quantity',
            'base_cost',
            'sell_price',
            'mrp',
            'time_to_ship',
            'width',
            'depth',
            'height',
            'product_group',
            'furniture_material',
            'fabric_type',
            'foam_type_density',
            'assembly',
            'warranty',
            'specification_general',
            'furniture_weight',
            'seating_height',
            'box_content',
            'box_count',
            'status',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>

</div>
