<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\furniture\models\Review;

/**
 * ReviewSearch represents the model behind the search form about `app\modules\MubAdmin\modules\furniture\models\Review`.
 */
class ReviewSearch extends Review
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'review_id', 'mub_user_id', 'approved_by'], 'integer'],
            [['email', 'name', 'rating', 'read', 'title', 'type', 'comment_text', 'status', 'approved_on', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find()->where(['del_status' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'review_id' => $this->review_id,
            'mub_user_id' => $this->mub_user_id,
            'approved_on' => $this->approved_on,
            'approved_by' => $this->approved_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'rating', $this->rating])
            ->andFilterWhere(['like', 'read', $this->read])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'comment_text', $this->comment_text])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
