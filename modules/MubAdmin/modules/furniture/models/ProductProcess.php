<?php 
namespace app\modules\MubAdmin\modules\furniture\models;

use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class ProductProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $product = new Product();
        $productCategories = new ProductCategories();
        $this->models = [
            'product' => $product,
            'productCategories' => $productCategories
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $category = new Category();
        $condition = ['status' => 'active'];
        $allcategory = $category->getAll('category_name',$condition);
        return [
            'allcategory' => $allcategory,
        ];
    }


    public function getSavedAttributes($attribute)
    {
        $attributes = [];
        foreach ($attribute as $key => $attr) 
        {
            $attributes[] = $attr->attribute_id;
        }
        return $attributes;
    }


    public function getSavedImages($image)
    {
        $images = [];
        foreach ($image as $key => $img) 
        {
            $images[] = $img->url;
        }
        return $images;
    }

    public function getSavedCategories($category)
    {
        $categories = [];
        foreach ($category as $key => $cat) 
        {
            $categories[] = $cat->category_id;
        }
        return $categories;
    }

    public function getRelatedModels($model)
    {
        $furnitureSelectedCategories = $this->getSavedCategories($model->categories);
        if(!empty($furnitureSelectedCategories))
        {
            $productCategories = $model->categories[0];
            if(count($furnitureSelectedCategories) == '1')
            {
                $productCategories->category_id = $furnitureSelectedCategories[0];
            }
            else
            {
                $productCategories->category_id = $furnitureSelectedCategories;            
            }
        }
        else
        {
            $productCategories = new ProductCategories();
        }

        $this->relatedModels = [
            'product' => $model,
            'productCategories' => $productCategories
        ];

        return $this->relatedModels;
    }

    public function saveProduct($product)
    {
        if(\Yii::$app->controller->action->id =='create')
        {
            $userId = \app\models\User::getMubUserId();
        }
        else
        {
            $userId = $product->mub_user_id;
        }
        $userId = \app\models\User::getMubUserId();
        $product->mub_user_id =  $userId;
        $product->product_slug = StringHelper::generateSlug($product->product_name);
    	return ($product->save()) ? $product->id : p($product->getErrors());
    }

    public function saveProductCategories($productCategories,$productId)
    {
        $selectedCategories = $productCategories->category_id;
        $mubUserId = \app\models\User::getMubUserId();
        $attribs = ['product_id','category_id','mub_user_id','created_at'];
        $recordSet = [];
        if (is_array($selectedCategories) || is_object($selectedCategories))
        {
        foreach ($selectedCategories as $categoryId) {
            $recordSet[] = [$productId,$categoryId,$mubUserId,date('Y-m-d h:m:s',time())];
        }
        }
        $catCount = count($selectedCategories);

        $productCategories->deleteAll(['product_id' => $productId]);
        $inserted = Yii::$app->db->createCommand()->batchInsert($productCategories->tableName(), $attribs, $recordSet)->execute();
        if($inserted == $catCount)
        {
            return true;
        }
        p($productCategories->getErrors());
    }

    public function saveProductImages($productId)
    {   
        $productImagesModel = new ProductImages();
        $currentUserId = \app\models\User::getMubUserId();
        $productImages = $productImagesModel::find()->where(['product_id' => NULL, 'mub_user_id' => $currentUserId])->all();
        foreach($productImages as $image)
        {   

            $image->product_id = $productId;
            if(!$image->save())
            {
                p($image->getErrors());
            }
        }
        return true;
    }
    
    public function saveData($data)
    {
    	if (isset($data['product'])&&
            isset($data['productCategories']))
            {
            try {
                   $productId = $this->saveProduct($data['product']);
                   if($productId)
                    {
                        $this->saveProductImages($productId);
                        $categories = $this->saveProductCategories($data['productCategories'],$productId);
                        if($categories)
                        {
                                return $productId;
                        }
                        p($data['productCategories']->getErrors());
                    }
                    p($data['product']->getErrors());
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}
