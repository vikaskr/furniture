<?php

namespace app\modules\MubAdmin\modules\furniture\controllers;

use Yii;
use app\components\MubController;
use app\modules\MubAdmin\modules\furniture\models\Rating;
use app\modules\MubAdmin\modules\furniture\models\RatingSearch;
use app\modules\MubAdmin\modules\furniture\models\RatingProcess;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RatingController implements the CRUD actions for Rating model.
 */
class RatingController extends MubController
{
   public function getPrimaryModel()
   {
        return new Rating();
   }

   public function getProcessModel()
   {
        return new RatingProcess();
   }

   public function getSearchModel()
   {
        return new RatingSearch();
   }
}