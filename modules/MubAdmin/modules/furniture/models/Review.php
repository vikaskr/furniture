<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use app\models\MubUser;
use app\models\State;

/**
 * This is the model class for table "product_review".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $email
 * @property string $name
 * @property string $rating
 * @property integer $review_id
 * @property string $read
 * @property string $title
 * @property string $type
 * @property string $comment_text
 * @property integer $mub_user_id
 * @property string $status
 * @property string $approved_on
 * @property integer $approved_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $approvedBy
 * @property MubUser $mubUser
 * @property Product $product
 * @property Review $review
 * @property Review[] $reviews
 */
class Review extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'review_id', 'mub_user_id', 'approved_by' , 'rating'], 'integer'],
            [['email', 'name'], 'required'],
            [['read', 'type', 'comment_text', 'status', 'del_status'], 'string'],
            [['approved_on', 'created_at', 'updated_at'], 'safe'],
            [['email', 'name','title'], 'string', 'max' => 255],
            [['approved_by'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['approved_by' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['review_id'], 'exist', 'skipOnError' => true, 'targetClass' => Review::className(), 'targetAttribute' => ['review_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'email' => 'Email',
            'name' => 'Name',
            'rating' => 'Rating',
            'review_id' => 'Review ID',
            'read' => 'Read',
            'title' => 'Title',
            'type' => 'Type',
            'comment_text' => 'Comment Text',
            'mub_user_id' => 'Mub User ID',
            'status' => 'Status',
            'approved_on' => 'Approved On',
            'approved_by' => 'Approved By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'approved_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::className(), ['id' => 'review_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['review_id' => 'id']);
    }
}
