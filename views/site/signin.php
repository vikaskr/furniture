<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
?>
</style>
<style type="text/css">.modal-footer {border-top: none!important; }</style>
<button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
<div class="login-popup-outer">
     <h3 class="magazines-by-sectors-heading log" style="color: #cc9600!important;"><strong>Login</strong></h3>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'frontend-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
            <div class="col-md-12 nopadding product-advertise-form" style="margin-top: 1em;">
              <div class="col-md-12 nopadding product-advertise-form-inner" style=" margin-left: 2em;">
                <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Username", 'class' => 'login-popup-panel-main input']); ?>
                <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Password", 'class' => 'login-popup-panel-main input']); ;?>
              </div>
              <div class="col-md-12 nopadding product-advertise-form-inner product-submit-button" style="margin-left: 6em; margin-bottom: 2em;
">
                <input type="submit" name="login" value="login" class="btn btn-success"> 
              </div>
              <div class="col-md-12 nopadding product-advertise-form-inner memnber-yet-con" style="color: #cc9600!important;"><span><a href="#" class="frontend-forget" id="forget">Forgot Password</a></span> <span> || <a href="#" class="signup-user login-button-nav" id="signup-modal"> Create Account</a></span></div>
            </div>
         <?php ActiveForm::end(); ?>
</div>

<div class="modal-footer">  
</div>
