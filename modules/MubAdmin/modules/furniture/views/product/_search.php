<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\furniture\products\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($product, 'id') ?>

    <?= $form->field($product, 'mub_user_id') ?>

    <?= $form->field($product, 'product_name') ?>

    <?= $form->field($product, 'product_slug') ?>

    <?= $form->field($product, 'product_description') ?>

    <?php // echo $form->field($product, 'product_excerpt') ?>

    <?php // echo $form->field($product, 'product_type') ?>

    <?php // echo $form->field($product, 'quantity') ?>

    <?php // echo $form->field($product, 'product_code') ?>

    <?php // echo $form->field($product, 'brand_name') ?>

    <?php // echo $form->field($product, 'price') ?>

    <?php // echo $form->field($product, 'cell_price') ?>

    <?php // echo $form->field($product, 'discount_price') ?>

    <?php // echo $form->field($product, 'status') ?>

    <?php // echo $form->field($product, 'created_at') ?>

    <?php // echo $form->field($product, 'updated_at') ?>

    <?php // echo $form->field($product, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
