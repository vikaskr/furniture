<?php

namespace app\modules\MubAdmin\modules\furniture\controllers;

use Yii;
use app\modules\MubAdmin\modules\furniture\models\Review;
use app\modules\MubAdmin\modules\furniture\models\ReviewSearch;
use app\modules\MubAdmin\modules\furniture\models\ReviewProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends MubController
{
     public function getPrimaryModel()
   {
        return new Review();
   }

   public function getProcessModel()
   {
        return new ReviewProcess();
   }

   public function getSearchModel()
   {
        return new ReviewSearch();
   }
   
   public function actionUpdateRating() {

        $response['success'] = false;

        if ( Yii::$app->request->post () ) {

            $id = Yii::$app->request->post ( 'id' );
            $model = Review::findOne ( $id );

            //update your database with the new rating 
            //............
            //get new rating in $newRating

            $response['rating'] = $newRating;
            $response['success'] = true;
        }
        echo Json::encode ( $response );
        Yii::$app->end ();
    }
}