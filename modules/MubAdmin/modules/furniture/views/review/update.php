<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $review app\modules\MubAdmin\modules\furniture\reviews\Review */

$this->title = 'Update Review: ' . $review->name;
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $review->name, 'url' => ['view', 'id' => $review->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="review-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'review' => $review,
     
    ]) ?>

</div>
