<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_social_platforms".
 *
 * @property integer $id
 * @property string $name
 * @property string $domain
 * @property string $slug
 * @property string $del_status
 *
 * @property MubUserSocial[] $mubUserSocials
 */
class MubSocialPlatforms extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_social_platforms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'domain', 'slug'], 'required'],
            [['del_status'], 'string'],
            [['name', 'domain', 'slug'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'domain' => Yii::t('app', 'Domain'),
            'slug' => Yii::t('app', 'Slug'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserSocials()
    {
        return $this->hasMany(MubUserSocial::className(), ['social_platform_id' => 'id'])->where(['del_status' => '0']);
    }
}
