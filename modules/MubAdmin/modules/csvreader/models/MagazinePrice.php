<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "magazine_price".
 *
 * @property integer $id
 * @property integer $magazine_id
 * @property integer $origin_id
 * @property string $price
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Magazine $magazine
 * @property Origin $origin
 */
class MagazinePrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'magazine_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['magazine_id', 'origin_id'], 'required'],
            [['magazine_id', 'origin_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'del_status'], 'string'],
            [['price','discount_percentage','featured'], 'string', 'max' => 255],
            [['magazine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Magazine::className(), 'targetAttribute' => ['magazine_id' => 'id']],
            [['origin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Origin::className(), 'targetAttribute' => ['origin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'magazine_id' => 'Magazine ID',
            'origin_id' => 'Origin ID',
            'price' => 'Price',
            'discount_percentage' => 'Discount Percentage',
            'featured' => 'Featured',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazine()
    {
        return $this->hasOne(Magazine::className(), ['id' => 'magazine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrigin()
    {
        return $this->hasOne(Origin::className(), ['id' => 'origin_id']);
    }
}
