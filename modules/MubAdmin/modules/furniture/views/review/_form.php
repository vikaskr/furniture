<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;
/* @var $this yii\web\View */
/* @var $review app\modules\MubAdmin\modules\furniture\reviews\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>
 <div class="row">
    <div class="col-md-5 col-md-offset-1">
    <?= $form->field($review, 'product_id')->textInput() ?>
 </div>
 <div class="col-md-5">
    <?= $form->field($review, 'email')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
    <div class="col-md-5 col-md-offset-1">
    <?= $form->field($review, 'name')->textInput(['maxlength' => true]) ?>
 </div>
 <div class="col-md-5">
  <?= $form->field($review, 'status')->dropDownList([ '0', '1', '2', '3', '4', ], ['prompt' => '']) ?>
</div>
</div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
    <?= $form->field($review, 'comment_text')->textarea(['rows' => 6]) ?>
</div>
</div>
<div class="row">
<div class="col-md-5 col-md-offset-1">
  
</div>
</div>
   <div class="row">
    <div class="col-md-5 col-md-offset-1">
    <div class="form-group">
        <?= Html::submitButton($review->isNewRecord ? 'Create' : 'Update', ['class' => $review->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
