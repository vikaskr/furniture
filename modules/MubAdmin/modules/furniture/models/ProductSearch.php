<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\furniture\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\modules\MubAdmin\modules\furniture\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'stock_quantity', 'base_cost', 'sell_price', 'mrp'], 'integer'],
            [['hsn_code', 'manufacture_SKU', 'manufacture_SKU_name', 'product_name', 'product_slug', 'description', 'inner_spec_of_seat', 'seat_mattress', 'arm_rest', 'time_to_ship', 'width', 'depth', 'height', 'product_group', 'furniture_material', 'fabric_type', 'foam_type_density', 'assembly', 'warranty', 'specification_general', 'furniture_weight', 'seating_height', 'box_content', 'box_count', 'status', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->where(['del_status' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'stock_quantity' => $this->stock_quantity,
            'base_cost' => $this->base_cost,
            'sell_price' => $this->sell_price,
            'mrp' => $this->mrp,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'hsn_code', $this->hsn_code])
            ->andFilterWhere(['like', 'manufacture_SKU', $this->manufacture_SKU])
            ->andFilterWhere(['like', 'manufacture_SKU_name', $this->manufacture_SKU_name])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_slug', $this->product_slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'inner_spec_of_seat', $this->inner_spec_of_seat])
            ->andFilterWhere(['like', 'seat_mattress', $this->seat_mattress])
            ->andFilterWhere(['like', 'arm_rest', $this->arm_rest])
            ->andFilterWhere(['like', 'time_to_ship', $this->time_to_ship])
            ->andFilterWhere(['like', 'width', $this->width])
            ->andFilterWhere(['like', 'depth', $this->depth])
            ->andFilterWhere(['like', 'height', $this->height])
            ->andFilterWhere(['like', 'product_group', $this->product_group])
            ->andFilterWhere(['like', 'furniture_material', $this->furniture_material])
            ->andFilterWhere(['like', 'fabric_type', $this->fabric_type])
            ->andFilterWhere(['like', 'foam_type_density', $this->foam_type_density])
            ->andFilterWhere(['like', 'assembly', $this->assembly])
            ->andFilterWhere(['like', 'warranty', $this->warranty])
            ->andFilterWhere(['like', 'specification_general', $this->specification_general])
            ->andFilterWhere(['like', 'furniture_weight', $this->furniture_weight])
            ->andFilterWhere(['like', 'seating_height', $this->seating_height])
            ->andFilterWhere(['like', 'box_content', $this->box_content])
            ->andFilterWhere(['like', 'box_count', $this->box_count])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
