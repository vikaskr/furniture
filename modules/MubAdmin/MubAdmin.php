<?php

namespace app\modules\MubAdmin;

class MubAdmin extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\MubAdmin\controllers';
    public $defaultRoute = 'dashboard';
    public function init()
    {
        parent::init();

        $this->modules = [
        'furniture' => [
            'class' => 'app\modules\MubAdmin\modules\furniture\Furniture',
            ],
        'gallery' => [
            'class' => 'app\modules\MubAdmin\modules\gallery\Gallery',
            ],
        'csvreader' => [
            'class' => 'app\modules\MubAdmin\modules\csvreader\CsvReader',
            ],
        'users' => [
            'class' => 'app\modules\MubAdmin\modules\users\User',
            ],
        ];
    }
}
