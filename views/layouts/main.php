<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    <!-- Meta tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">.products{
        background-color: #fff!important;
    }</style>
</head>

<body>
   
   <!--  <div class="page-loader"></div> -->

    <div class="wrapper">

        <!-- ======================== Navigation ======================== -->

        <nav class="navbar-fixed">

            <div class="container">

                <!-- ==========  Top navigation ========== -->

                <div class="navigation navigation-top clearfix">
                    <ul>
                        <!--add active class for current page-->
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                         <?php if(Yii::$app->user->isGuest){?><li class="login-button-nav login-nav-outer"><a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal">Login</a></li><li class="login-button-nav login-nav-outer hidden-lg hidden-md hidden-sm hidden-xs"><a href="/site/signup" id="signup-modal" data-toggle="modal" data-target="#myModal">Login</a></li><?php } else {?>
                <li class="login-button-nav login-nav-outer"><a href="<?= Url::to(['/site/logout'])?>" data-method="post" >Logout</a></li>
                <?php }?>
                       <!--  <li><a href="#" class="open-search"><i class="icon icon-magnifier"></i></a></li> -->
                        <li><a href="/site/checkout" class="open-cart" scroll={false}><i class="icon icon-cart"></i></a></li><span class="cartcount"><?php echo $this->render('/site/cartcount');?></span>
                    </ul>
                </div>

                <!-- ==========  Main navigation ========== -->

                <div class="navigation navigation-main">
                    <a href="/" class="logo"><img src="/images/logo.png" alt="" /></a>
                    <a href="#" class="open-login"><i class="icon icon-user"></i></a>
                    <a href="#" class="open-search"><i class="icon icon-magnifier"></i></a>
                    <a href="/site/checkout" class="open-cart"><i class="icon icon-cart"></i> </a>
                    <a href="#" class="open-menu"><i class="icon icon-menu"></i></a>
                    <div class="floating-menu">
                        <!--mobile toggle menu trigger-->
                        <div class="close-menu-wrapper">
                            <span class="close-menu"><i class="icon icon-cross"></i></span>
                        </div>
                        <ul>

                            <li>
                                <a href="/site/category?name=sofa">Sofa <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown">
                                    <div class="navbar-box">
                                        <div class="box-1">
                                            <div class="box">
                                                <div class="h2">Sofa</div>
                                                <div class="clearfix">
                                                    <p>Homes that differ in terms of style, concept and architectural solutions have been furnished by Furniture Factory. These spaces tell of an international lifestyle that expresses modernity, research and a creative spirit.</p>
                                                    <a class="btn btn-clean btn-big" href="/site/category?name=sofa">Shop now</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-2">
                                            <div class="box clearfix">
                                               <ul>
                                                   <li class="label">Sofa Variety</a></li>
                                               </ul>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=fabric-sofa-sets">Fabric Sofa Sets</a></li>
                                                            <li><a href="site/category?name=wooden-sofa-sets">Wooden Sofa Sets</a></li>
                                                            <li><a href="site/category?name=3-seater-sofa">3 Seater Sofa</a></li>
                                                            <li><a href="site/category?name=2-seater-sofa">2 Seater Sofa</a></li>
                                                            <li><a href="site/category?name=leatherette-sofa">Leatherette Sofa</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=chesterfield-sofas">Chesterfield Sofas</a></li>
                                                            <li><a href="site/category?name=futonslove-seats">Futons/Love Seats</a></li>
                                                            <li><a href="site/category?name=sectional-sofas">Sectional Sofas</a></li>
                                                            <li><a href="site/category?name=sofa-cum-bed">Sofa Cum Bed</a></li>
                                                            <li><a href="site/category?name=lounger-chairs">Lounger Chairs</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="/site/category?name=dining">Dining <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown">
                                    <div class="navbar-box">
                                        
                                        <div class="box-2">
                                            <div class="box clearfix">
                                               <ul>
                                                   <li class="label">Dining Room Furniture Variety</a></li>
                                               </ul>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=6-seater-dining-set">6 Seater Dining Sets</a></li>
                                                            <li><a href="site/category?name=4-seater-dining-sets">4 Seater Dining Sets</a></li>
                                                            <li><a href="site/category?name=roundoval-dining-sets">Round/Oval Dining Sets</a></li>
                                                            <li><a href="site/category?name=dining-chairs">Dining Chairs</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=dining-bench">Dining Bench</a></li>
                                                            <li><a href="site/category?name=bar-cabinets">Bar Cabinets</a></li>
                                                            <li><a href="site/category?name=bar-trolleys">Bar Trolleys</a></li>
                                                            <li><a href="site/category?name=wine-racks">Wine Racks</a></li>
                                                            <li><a href="site/category?name=bar-stools--chairs">Bar Stools & Chairs</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-1">
                                            <div class="box">
                                                <div class="h2">Dining</div>
                                                <div class="clearfix">
                                                    <p>Homes that differ in terms of style, concept and architectural solutions have been furnished by Furniture Factory. These spaces tell of an international lifestyle that expresses modernity, research and a creative spirit.</p>
                                                    <a class="btn btn-clean btn-big" href="/site/category?name=dining">Shop now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                              <li>
                                <a href="/site/category?name=living">Living <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown">
                                    <div class="navbar-box">
                                         <div class="box-1">
                                            <div class="box">
                                                <div class="h2">Living</div>
                                                <div class="clearfix">
                                                    <p>Homes that differ in terms of style, concept and architectural solutions have been furnished by Furniture Factory. These spaces tell of an international lifestyle that expresses modernity, research and a creative spirit.</p>
                                                    <a class="btn btn-clean btn-big" href="/site/category?name=living">Shop now</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-2">
                                            <div class="box clearfix">
                                               <ul>
                                                   <li class="label">Living Room Furniture Variety</a></li>
                                               </ul>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=tv-units">TV Units</a></li>
                                                            <li><a href="site/category?name=bookshelves">Bookshelves</a></li>
                                                            <li><a href="site/category?name=shoe-racks">Shoe Racks</a></li>
                                                            <li><a href="site/category?name=prayer-units">Prayer Units</a></li>
                                                            <li><a href="site/category?name=magazine-racks">Magazine Racks</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=wall-shelves">Wall Shelves</a></li>
                                                            <li><a href="site/category?name=coffee-tables">Coffee Tables</a></li>
                                                            <li><a href="site/category?name=nest-of-tables">Nest Of Tables</a></li>
                                                            <li><a href="site/category?name=side-=end-tables">Side & End Tables</a></li>
                                                            <li><a href="site/category?name=console-table">Console Table</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="/site/category?name=bedroom">BedRoom<span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown">
                                    <div class="navbar-box">
                                        
                                        <div class="box-2">
                                            <div class="box clearfix">
                                               <ul>
                                                   <li class="label">BedRoom Furniture Variety</a></li>
                                               </ul>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=queen-size-beds">Queen Size Beds</a></li>
                                                            <li><a href="site/category?name=king-size-beds">King Size Beds</a></li>
                                                            <li><a href="site/category?name=single-beds">Single Beds</a></li>
                                                            <li><a href="site/category?name=hydraulic-storage-beds">Hydraulic Storage Beds</a></li>
                                                            <li><a href="site/category?name=upholstered-beds">Upholstered Beds</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li><a href="site/category?name=sofa-cum-beds">Sofa Cum Beds</a></li>
                                                            <li><a href="site/category?name=bedside-bable">Bedside Table</a></li>
                                                            <li><a href="site/category?name=chest-of-drawers">Chest Of Drawers</a></li>
                                                            <li><a href="site/category?name=wardrobes">Wardrobes</a></li>
                                                            <li><a href="site/category?name=dressing-table">Dressing Table</a></li>
                                                            <li><a href="site/category?name=trunk--blanket-boxes">Trunk & Blanket Boxes</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-1">
                                            <div class="box">
                                                <div class="h2">BedRoom</div>
                                                <div class="clearfix">
                                                    <p>Homes that differ in terms of style, concept and architectural solutions have been furnished by Furniture Factory. These spaces tell of an international lifestyle that expresses modernity, research and a creative spirit.</p>
                                                    <a class="btn btn-clean btn-big" href="/site/category?name=bedroom">Shop now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="/site/category?name=storage">Storage <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown navbar-dropdown-single">
                                    <div class="navbar-box">
                                        <div class="box-2">
                                            <div class="box clearfix">
                                                <ul>
                                                    <li class="label">Storage</li>
                                                    <li><a href="site/category?name=tv-units">TV Units</a></li>
                                                    <li><a href="site/category?name=bookshelves">Bookshelves</a></li>
                                                    <li><a href="site/category?name=display-units">Display Units</a></li>
                                                    <li><a href="site/category?name=shoe-racks">Shoe Racks</a></li>
                                                    <li><a href="site/category?name=wall-shelves">Wall Shelves</a></li>
                                                    <li><a href="site/category?name=prayer-units">Prayer Units</a></li>
                                                    <li><a href="site/category?name=magazine-racks">Magazine Racks</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                             <li>
                                <a href="/site/category?name=study">Study <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown navbar-dropdown-single">
                                    <div class="navbar-box">
                                        <div class="box-2">
                                            <div class="box clearfix">
                                                <ul>
                                                    <li class="label">Study</li>
                                                    <li><a href="site/category?name=study-tables">Study Tables</a></li>
                                                    <li><a href="site/category?name=laptop-tables">Laptop Tables</a></li>
                                                    <li><a href="site/category?name=kids-study-tables">Kids Study Tables</a></li>
                                                    <li><a href="site/category?name=computer-tables">Computer Tables</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li><a href="/site/category?name=decor">Decor <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown navbar-dropdown-single">
                                    <div class="navbar-box">
                                        <div class="box-2">
                                            <div class="box clearfix">
                                                <ul>
                                                    <li class="label">Decor</li>
                                                    <li><a href="site/category?name=mirror-frames">Mirror Frames</a></li>
                                                    <li><a href="site/category?name=wall-hangings">Wall Hangings</a></li>
                                                    <li><a href="site/category?name=cushions">Cushions</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li><a href="/site/category?name=furniture-for-projects-b2b">Office Furniture<span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                <div class="navbar-dropdown navbar-dropdown-single" style="margin-right: -27px!important;">
                                    <div class="navbar-box">
                                        <div class="box-2">
                                            <div class="box clearfix">
                                                <ul>
                                                    <li class="label">Office Furniture</li>
                                                    <li><a href="site/category?name=restaurant-furniture">Restaurant Furniture</a></li>
                                                    <li><a href="site/category?name=office-furnitures">Office Furniture</a></li>
                                                    <li><a href="site/category?name=full-house-furnitures">Full House Furniture</a></li>
                                                    <li><a href="site/category?name=hotel-furniture">Hotel Furniture</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

                <!-- ==========  Search wrapper ========== -->

                <div class="search-wrapper">
                    <input class="form-control" placeholder="Search..." />
                    <button class="btn btn-main">Go!</button>
                </div>

                <!-- ==========  Login wrapper ========== -->

                <div class="login-wrapper">
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <a href="#forgotpassword" class="open-popup">Forgot password?</a>
                            <a href="#createaccount" class="open-popup">Don't have an account?</a>
                        </div>
                        <button type="submit" class="btn btn-block btn-main">Submit</button>
                    </form>
                </div>

                <!-- ==========  Cart wrapper ========== -->

              
            </div>
        </nav>

<?= $content ?>
    <footer>
            <div class="container">

                <!--footer showroom-->
                <div class="footer-showroom">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Visit our showroom</h2>
                            <p>200 12th Ave, New York, NY 10001, USA</p>
                            <p>Mon - Sat: 10 am - 6 pm &nbsp; &nbsp; | &nbsp; &nbsp; Sun: 12pm - 2 pm</p>
                        </div>
                        <div class="col-sm-4 text-center">
                            <a href="#" class="btn btn-clean"><span class="icon icon-map-marker"></span> Get directions</a>
                            <div class="call-us h4"><span class="icon icon-phone-handset"></span> 333.278.06622</div>
                        </div>
                    </div>
                </div>

                <!--footer links-->
                <div class="footer-links">
                    <div class="row">
                        <div class="col-sm-4 col-md-2">
                            <h5>Browse by</h5>
                            <ul>
                                <li><a href="#">Brand</a></li>
                                <li><a href="#">Product</a></li>
                                <li><a href="#">Category</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-2">
                            <h5>Recources</h5>
                            <ul>
                                <li><a href="#">Design</a></li>
                                <li><a href="#">Projects</a></li>
                                <li><a href="#">Sales</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-2">
                            <h5>Our company</h5>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <h5>Sign up for our newsletter</h5>
                            <p><i>Add your email address to sign up for our monthly emails and to receive promotional offers.</i></p>
                            <div class="form-group form-newsletter">
                                <input class="form-control" type="text" name="email" value="" placeholder="Email address" />
                                <input type="submit" class="btn btn-clean btn-sm" value="Subscribe" />
                            </div>
                        </div>
                    </div>
                </div>

                <!--footer social-->

                <div class="footer-social">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="https://themeforest.net/item/mobel-furniture-website-template/20382155" target="_blank"><i class="fa fa-download"></i> Download Mobel</a> &nbsp; | <a href="#">Sitemap</a> &nbsp; | &nbsp; <a href="#">Privacy policy</a>
                        </div>
                        <div class="col-sm-6 links">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

  <div class="modal fade login-popup-panel-main" id="myModal" tabindex="-1" role="dialog" style="margin-left: 17em;">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info" id="dynamic-modal">
              
        </div>
    </div>
  </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
