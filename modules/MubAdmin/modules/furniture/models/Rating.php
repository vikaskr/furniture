<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "rating".
 *
 * @property int $id
 * @property string $icon_name
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 */
class Rating extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['icon_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon_name' => 'Icon Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}
