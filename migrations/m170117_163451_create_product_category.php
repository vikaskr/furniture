<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163451_create_product_category extends Migration
{
    public function getTableName()
    {
        return 'product_category';
    }
    public function getForeignKeyFields()
    {
        return [];
    }

    public function getKeyFields()
    {
        return [
            'category_name'  =>  'category_name',
            'status' => 'status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'category_name' => $this->string()->notNull(),
            'category_slug' => $this->string()->notNull(),
            'icon_name' => $this->string(),
            'description' => $this->string(),
            'tag_line' => $this->string(),
            'image' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

     public function safeUp()
    {
        parent::safeUp();
        $columns = ['category_name','status','del_status'];
        $this->db->createCommand()->createIndex('unique_category_name_status', $this->getTableName(), $columns, true)->execute();
    }
}
