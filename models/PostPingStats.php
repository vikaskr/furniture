<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_ping_stats".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $ping_id
 * @property string $status
 * @property string $response_text
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubPingUrls $ping
 * @property Post $post
 */
class PostPingStats extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_ping_stats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'ping_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['response_text'], 'string', 'max' => 255],
            [['ping_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubPingUrls::className(), 'targetAttribute' => ['ping_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'ping_id' => Yii::t('app', 'Ping ID'),
            'status' => Yii::t('app', 'Status'),
            'response_text' => Yii::t('app', 'Response Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPing()
    {
        return $this->hasOne(MubPingUrls::className(), ['id' => 'ping_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id'])->where(['del_status' => '0']);
    }
}
