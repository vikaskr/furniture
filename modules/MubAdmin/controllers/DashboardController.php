<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\MubUser;
use app\models\MubUserSearch;
use yz\shoppingcart\ShoppingCart;
use app\components\Model;
use yii\helpers\ArrayHelper;

class DashboardController extends MubController
{
	public function getProcessModel()
	{
        return new BlogProcess();
	}

	public function getPrimaryModel()
	{
        return new MubUser();
	}

    public function getSearchModel()
    {
        return new MubUserSearch();
    }

	public function actionImageUpload()
	{
		$file = \Yii::$app->getBodyParams();
		p($file);
	}

	public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }
    public function saveBookingDetails($postData)
    {
         $cart = new ShoppingCart();
         $cartItems = ArrayHelper::toArray($cart->getPositions());
         $price = $cart->getCost();
        if(!empty($postData))
        {
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking->first_name = $postData['firstname'];
            $booking->last_name = $postData['lastname'];
            $booking->mub_user_id = $mubUserId;
            $booking->email = $postData['email'];
            $booking->txn_id = $postData['txnid'];
            $booking->mobile = $postData['phone'];
            $booking->product = $postData['product'];
            $booking->city = $postData['city'];
            $booking->state = $postData['state'];
            $booking->country = $postData['country'];
            $booking->pin_code = $postData['pin_code'];
            $booking->status = 'booking';
            $booking->amount = $postData['amount'];
            $booking->quantity = $postData['quantity'];
            $booking->delivery_address = $postData['delivery_address'];
            
            $add = $booking->delivery_address;
            if(!$booking->save())
            {
               p($booking->getErrors());
            }
            $itemName = [];
            foreach ($cartItems as $value) 
            {
                $itemName[] = $value['product_name'];
            }

           $success = \Yii::$app->mailer->compose('Listhomemail',['cartItems' => $cartItems, 'price' => $price, 'add' => $add])
                    ->setFrom('info@mayaexpress.in')
                    ->setCc('praveen@makeubig.com')
                    ->setBcc('info@mayaexpress.in','MayaExpress ADMIN')
                    ->setTo($booking->email)->setSubject('Your Booked Order')
                    ->send();    

                return $success;
        }
        return true;
    }

    public function actionBookingCod()
    {
         $cart = new ShoppingCart();
         $cartItems = ArrayHelper::toArray($cart->getPositions());
         $price = $cart->getCost();
         $cod = \Yii::$app->request->getBodyParams();

           $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking->first_name = $cod['firstname'];
            $booking->last_name = $cod['lastname'];
            $booking->mub_user_id = $mubUserId;
            $booking->email = $cod['email'];
            $booking->mobile = $cod['phone'];
            $booking->product = $cod['product'];
            $booking->city = $cod['city'];
            $booking->state = $cod['state'];
            $booking->country = $cod['country'];
            $booking->pin_code = $cod['pin_code'];
            $booking->status = 'COD';
            $booking->amount = $cod['price'];
            $booking->quantity = $cod['quantity'];
            $booking->delivery_address = $cod['delivery_address'];
            
            $add = $booking->delivery_address;

            if(!$booking->save())
            {
               p($booking->getErrors());
            }
            $itemName = [];
            foreach ($cartItems as $value) 
            {
                $itemName[] = $value['product_name'];
            }

            $success = \Yii::$app->mailer->compose('Listhomemail',['cartItems' => $cartItems, 'add' => $add])
                    ->setFrom('info@mayaexpress.in')
                    ->setCc('praveen@makeubig.com')
                    ->setBcc('info@mayaexpress.in','MayaExpress ADMIN')
                    ->setTo($booking->email)->setSubject('Your Booked Order')
                    ->send(); 
                return $success;

    }

    public function actionPayuHash()
    {
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            $postData = \Yii::$app->request->getBodyParams();
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';  
            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($postData[$hash_var]) ? $postData[$hash_var] : '';
                $hash_string .= '|';
            }
            //add merchant salt here
          $hash_string .= 'AJi6ZxOUP2';
          $result['hash'] = strtolower(hash('sha512', $hash_string));
          $this->saveBookingDetails($postData);
          return $result;
        }
    }
}