<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\furniture\categorys\Category */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <div class="row">
    	<div class="col-md-5 col-md-offset-1">
    <?= $form->field($category, 'icon_name')->textInput(['maxlength' => true]); ?>
    </div>
    <div class="col-md-5">
    <?= $form->field($category, 'category_name')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
   <div class="row">
        <div class="col-md-5 col-md-offset-1">

            <?= $form->field($category, 'description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($category, 'tag_line')->textInput(['maxlength' => true]); ?>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-5 col-md-offset-1">

            <?= $form->field($category, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($category, 'image')->fileInput(); ?>
        </div>
                <?php if(\Yii::$app->controller->action->id == 'update'){?>
            <div class="row text-center">
                <div class="col-md-6">
                    <img src="/<?= $category->image;?>" class="img-responsive" width="200">
                </div>
            </div>
            <?php }?>
    </div>
  <div class="row">
  	<div class="col-md-5 col-md-offset-1">
    <div class="form-group">
        <?= Html::submitButton($category->isNewRecord ? 'Create' : 'Update', ['class' => $category->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
 </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
<br/><br/><br/><br/>
  <div class="panel panel-default" id="icons-furniture">
    <div class="panel-heading">Furniture icons</div>
    <div class="panel-body">
        <div class="row icons-wrapper">
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-sofa"></i>
                    <figcaption>f-icon-sofa</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-armchair"></i>
                    <figcaption>f-icon-armchair</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-chair"></i>
                    <figcaption>f-icon-chair</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-dining-table"></i>
                    <figcaption>f-icon-dining-table</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-media-cabinet"></i>
                    <figcaption>f-icon-media-cabinet</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-table"></i>
                    <figcaption>f-icon-table</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-bookcase"></i>
                    <figcaption>f-icon-bookcase</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-bedroom"></i>
                    <figcaption>f-icon-bedroom</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-nightstand"></i>
                    <figcaption>f-icon-nightstand</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-children-room"></i>
                    <figcaption>f-icon-children-room</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-kitchen"></i>
                    <figcaption>f-icon-kitchen</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-bathroom"></i>
                    <figcaption>f-icon-bathroom</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-wardrobe"></i>
                    <figcaption>f-icon-wardrobe</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-shoe-cabinet"></i>
                    <figcaption>f-icon-shoe-cabinet</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-office"></i>
                    <figcaption>f-icon-office</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-bar-set"></i>
                    <figcaption>f-icon-bar-set</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-lightning"></i>
                    <figcaption>f-icon-lightning</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-carpet"></i>
                    <figcaption>f-icon-carpet</figcaption>
                </figure>
            </div>
            <div class="col-sm-3 col-xs-6">
                <figure>
                    <i class="f-icon f-icon-accessories"></i>
                    <figcaption>f-icon-accessories</figcaption>
                </figure>
            </div>
        </div>
    </div>
</div>
