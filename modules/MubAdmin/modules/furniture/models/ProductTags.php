<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;

/**
 * This is the model class for table "product_tags".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $tag_id
 * @property string $tag_name
 * @property string $tag_slug
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Product $product
 * @property MubTag $tag
 */
class ProductTags extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'tag_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['tag_name', 'tag_slug'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubTag::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'tag_id' => 'Tag ID',
            'tag_name' => 'Tag Name',
            'tag_slug' => 'Tag Slug',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(MubTag::className(), ['id' => 'tag_id']);
    }
}
