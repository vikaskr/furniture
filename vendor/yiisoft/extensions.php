<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'yiister/yii2-gentelella' => 
  array (
    'name' => 'yiister/yii2-gentelella',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@yiister/gentelella' => $vendorDir . '/yiister/yii2-gentelella',
    ),
  ),
  '2amigos/yii2-tinymce-widget' => 
  array (
    'name' => '2amigos/yii2-tinymce-widget',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@dosamigos/tinymce' => $vendorDir . '/2amigos/yii2-tinymce-widget/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'omnilight/yii2-shopping-cart' => 
  array (
    'name' => 'omnilight/yii2-shopping-cart',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@yz/shoppingcart' => $vendorDir . '/omnilight/yii2-shopping-cart',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '1.0.8.0',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.9.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating',
    ),
  ),
);
