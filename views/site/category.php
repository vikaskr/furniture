<?php 
use \app\helpers\ImageUploader;
use \app\modules\MubAdmin\modules\furniture\models\Category;
use \app\modules\MubAdmin\modules\furniture\models\ProductCategories;
use yii\data\Pagination;
use yii\widgets\LinkPager;

$productImg = new \app\modules\MubAdmin\modules\furniture\models\ProductImages();
$product = new \app\modules\MubAdmin\modules\furniture\models\Product();

?>
<style type="text/css">
    select{
        width: 100%!important;
    }
    .filters .filter-box .title:after {
        display: none;
    }
</style>

        <!-- ======================== Main header ======================== -->

        <section class="main-header" style="background-image:url(/images/9.jpg)">
            <header>
                <div class="container">
                    <h1 class="h2 title">Shop</h1>
                    <ol class="breadcrumb breadcrumb-inverted">
                        <li><a href="/"><span class="icon icon-home"></span></a></li>
                        <li><a href="#">Product Category</a></li>
                    </ol>
                </div>
            </header>
        </section>

        <!-- ========================  Icons slider ======================== -->

        <section class="owl-icons-wrapper">

            <!-- === header === -->

            <header class="hidden">
                <h2>Product categories</h2>
            </header>

            <div class="container">

                <div class="owl-icons">

                    <!-- === icon item === -->
                    <?= $this->render('allcategory');?>
                </div> <!--/owl-icons-->
            </div> <!--/container-->
        </section>

   <form id="pricesort" method="post">
       <div class="filters filters-top">
            <div class="container">
                <div class="row" style="padding: 15px; background: lightgrey;">
                    <div class="col-md-6 col-xs-12">
                     <h3 style="margin-top: 16px!important;"><span>Showing <strong><?= count($productData);?></strong> of <strong><?= $countQuery;?></strong> items</span></h3>
                    </div>
                     <div class="col-md-2"></div>
                    <div class="col-md-4 col-xs-12">
                        <div class="filter-box">
                            <div class="title">
                                <select name="filters" id="priceFilter">
                                  <option>Select Price Range</option>
                                  <option value="plth">Price Low to High</option>
                                  <option value="phtl">Price High to Low</option>
                                </select>
                            </div>
                            <input type="hidden" name="category" value="<?= $catName;?>" id="id">
                        </div> 
                    </div>
                </div>
            </div>
        </div> 
    </form> 

        <!-- ======================== Products ======================== -->

        <section class="products" id="price-filter">

            <div class="container">

                <div class="row">

                    <div class="col-md-12 col-xs-12">

                        <div class="row" style="padding-bottom: 20px; margin-top: -45px;">
                           <?php 
                           foreach ($productData as $value) { 
                            $relPro= $value['id'];
                            $relImg = $productImg::find()->where(['del_status' => '0','product_id' => $relPro])->one();
                            ?>
                           <div class="col-md-6 col-xs-6">
                                <article>
                                    <div class="figure-grid">
                                        <div class="image">
                                            <a href="/site/product?name=<?= $value['product_slug']?>">
                                                <?php if(!empty($relImg->thumbnail_url)){?>
                                                <img src="<?= ImageUploader::resizeRender($relImg->thumbnail_url, '533', '380'); ?>" alt="" style="height: 360px;" />
                                                <?php }else{?>
                                                <img src="/images/not-found.png" alt="" style="height: 360px;" />
                                                <?php }?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="figure-grid">
                                         <div class="text">
                                            <h2 class="title h5"><a href="/site/product?name=<?= $value['product_slug']?>"><?= $value['product_name']?></a></h2> <button class="pull-right btn btn-small" style="background-color: #cc9600;"><a href="/site/product?name=<?= $value['product_slug']?>" style="color:#fff;">View Detail</a></button>
                                            <sub class="fontsize">Retail Price</sub><br/>
                                          <!--   <sup class="fontsize">Offer Price</sup> -->
                                           <!--  <sub>₹ <?= $value['mrp']?>,</sub> -->
                                            <sup>₹ <?= $value['mrp']?></sup><br/>
                                            <br>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <?php }?>
                        </div>
                    </div> <!--/product items-->
                </div><!--/row-->
            </div><!--/container-->
                <div class="row"><div class="col-md-2"></div><div class="col-md-8"><center><?= LinkPager::widget([
                             'pagination' => $pages,
                             ]);
                            ?></center></div>
                </div>
        </section>
