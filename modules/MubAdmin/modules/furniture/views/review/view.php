<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $review app\modules\MubAdmin\modules\furniture\reviews\Review */

$this->title = $review->name;
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6 col-md-offset-1">
<div class="review-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $review->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $review->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $review,
        'attributes' => [
            'id',
            'product_id',
            'email:email',
            'name',
            'rating',
            'review_id',
            'read',
            'title',
            'type',
            'comment_text:ntext',
            'mub_user_id',
            'status',
            'approved_on',
            'approved_by',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>
</div>
</div>
</div>
