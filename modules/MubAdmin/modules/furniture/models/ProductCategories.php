<?php

namespace app\modules\MubAdmin\modules\furniture\models;

use Yii;
use app\models\MubUser;
use app\models\State;

/**
 * This is the model class for table "product_categories".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $product_id
 * @property integer $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property ProductCategory $category
 * @property MubUser $mubUser
 * @property Product $product
 */
class ProductCategories extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['mub_user_id', 'product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'product_id' => 'Product ID',
            'category_id' => 'Category ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdcutCategories()
    {
        return $this->hasOne(ProductCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
