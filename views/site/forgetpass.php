<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css"></style>
</style>


<div class="login-popup-outer container" style="margin-left: -25em;"> 
     <h3 class="magazines-by-sectors-heading " style="margin-top: 1em; margin-right: 40px; margin-bottom: 40px; text-align: center; color: #cc9600!important;"><strong>Forget Password</strong></h3>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'frontend-forget','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
            <div class="col-lg-12 nopadding product-advertise-form" style="margin-top: -1em;">
              <div class="col-lg-12 nopadding product-advertise-form-inner">
                <div class='col-sm-10 col-sm-offset-1' style="text-align: center; margin-left: 67px;">
               <?= $form->field($model, 'email')->textInput()->input('email', ['placeholder' => "Registered Email ID", 'class' => 'login-popup-panel-main input'])->label(false); ?></div>
              </div>
              <div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button" style="padding: 1em; margin-left: -20px; text-align: center">
                <input type="submit" class="btn btn-success" name="login" value="Submit">
              </div><br>
              <div class="col-xs-12 nopadding product-advertise-form-inner memnber-yet-con" style="text-align: center; margin-left: -20px; margin-bottom: 1em;"><span></span> <span>Not a member? <a href="#" class="signup-user login-button-nav" id="signup-modal" style="color: #cc9600!important;"> Register</a></span></div>
            </div>
         <?php ActiveForm::end(); ?>
</div>

<div class="modal-footer">  
</div>

