<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "origin".
 *
 * @property integer $id
 * @property string $continents
 * @property string $origin_name
 * @property string $origin_slug
 * @property string $origin_status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Currency[] $currencies
 * @property Magazine[] $magazines
 * @property MagazinePrice[] $magazinePrices
 */
class Origin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'origin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin_status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['continents', 'origin_name', 'origin_slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'continents' => 'Continents',
            'origin_name' => 'Origin Name',
            'origin_slug' => 'Origin Slug',
            'origin_status' => 'Origin Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['origin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazines()
    {
        return $this->hasMany(Magazine::className(), ['origin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazinePrices()
    {
        return $this->hasMany(MagazinePrice::className(), ['origin_id' => 'id']);
    }
}
