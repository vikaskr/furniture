<?php

namespace app\migrations;
use app\commands\Migration;

/**
 * Class m180203_120804_product_attributes
 */
class m180203_120804_product_attributes extends Migration
{
    public function getTableName()
    {
        return 'product_attributes';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'product_id' => ['product','id'],
        ];
    }

    public function getKeyFields()
    {
        return [];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'attribute_name' => $this->string()->notNull(),
            'attribute_value' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}