<?php 
  use yii\widgets\ActiveForm;
  ?>
<section class="category-banner-panel">

</section>
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
        <ul>
        <li><a>Wide Range</a></li>
        <li><a>Great prices</a></li>
        <li><a>Best Discounts</a></li>
        <li><a>Quick Delivery</a></li>
        <li><a>24x7 Customer care</a></li>
        <li><a>Free Shipping</a></li>
        <li><a>Easy Renewals</a></li>
        </ul>
  </div>
</div>


<section class="refund-policy-content contact-us-content">
	<div class="col-xs-12 nopadding main-inner-heading"><h1>Contact Us</h1></div>
        	<div class="col-xs-12 refund-content-inner">
            	
                 <div class="col-xs-12 get-in-touch">
                 	<h3>GET IN TOUCH WITH US</h3>
                  <p>If You Need Any Assistance, With Your Selection Or Order, You Can Write To Customer Support. We Provide 24 X 7 Hours Online Customer Care Service To Our Customers, Connect With Us Any Time, Any Day, We Are Always At Your Service. Your Satisfaction Is Our Goal And We're Dedicated To Achieve It!</p>
                 </div>
                 <div class="col-xs-12 contact-us-form-outer">
                 	  <div class="col-md-8 col-sm-8 col-xs-12 contact-left-form" style="margin-top: -68px!important;">
                      	   <div class="col-xs-12 contact-left-form-img">
                      	   		<img src="images/Write-us.jpg" alt="">
                   		   </div>
                           <div class="col-xs-12 contact-left-form-inner">
                      	   		<?php $form = ActiveForm::begin(['id' => 'contact_magazine']); ?>
                                <div class="row">
                                    <div class="contact-subject-field" style="width: 48%!important;padding-left:18px; ">
                                        <?= $form->field($contact, 'name')->textInput([ 'placeholder' => "Name (required)", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                                    </div>
                                    <div class="contact-subject-field" style="width: 50%!important; padding-left:20px; ">
                                         <?= $form->field($contact, 'email')->textInput([ 'placeholder' => "Email (required)", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="contact-subject-field" style="width: 48%!important;padding-left:18px; ">
                                        <?= $form->field($contact, 'phone')->textInput([ 'placeholder' => "Mobile (required)", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                                    </div>
                                    <div class="contact-subject-field" style="width: 50%!important; padding-left:20px; ">
                                        <?= $form->field($contact, 'subject')->textInput([ 'placeholder' => "Subject (required)", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                                    </div>
                                </div>
                                    
                                    <div class="contact-subject-field"> 
                                         <?= $form->field($contact, 'message')->textarea([ 'placeholder' => "Message (required)", 'class' => 'login-popup-panel-main input'])->label(false); ?></div>
                                    <div class="contact-subject-field"><input name="submit" type="submit"></div>
                                <?php ActiveForm::end();?>
                   		   </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-4 col-xs-12 contact-side-bar">
                            <h3>INFORMATION</h3>						
                            <div class="vina-addon-content">
                                <span><i class="fa fa-map-marker"></i>Customer Service</span>
                            </div>
                         	<h3> MAGAZINES WORLD</h3>
                            <div class="vina-addon-content">
                                <ul>
                                	<li>Online Publications Consultants</li>
                                    <li>DLF Cyber City . Building No. 5</li>
                                    <li>Level 18 Tower A . Phase III</li>
                                    <li>Gurgaon – 122002</li>
                                    <li>Haryana . India</li>
                                    <li><i class="fa fa-envelope"></i>Contact@MagazinesWorld.Online</li>
                                    <li><i class="fa fa-phone"></i>+91 9773723591 | +91 9773723592</li>
                                </ul>
                                <h3>BUSINESS HOURS</h3>
                                <div class="vina-addon-content">
                                	<ul>
                                       <li>Monday – Friday: 9 A.M to 5 P.M</li>
                                        <li>Saturday-Sunday: Off</li>
                                        <li>Online Customer Care: 24 x 7</li>
                                    </ul>
                                    <ul class="social-icons">
                                        <li><a class="facebook social-icon" title="Facebook" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twitter social-icon" title="Twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        <li></li>
                                        </ul>
                                </div>
                                <div class="vina-addon-content">
                                	<img src="images/Office-Gurgaon-Haryana-1.jpg" alt="">
                                </div>

                            </div>
                      </div>
                 </div>                  
                 </div>
            </div>
</section>