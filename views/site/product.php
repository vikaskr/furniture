<?php 

use \app\modules\MubAdmin\modules\furniture\models\ProductImages;
use \app\modules\MubAdmin\modules\furniture\models\ProductCategories;
use \app\helpers\ImageUploader;
use yz\shoppingcart\ShoppingCart;

$cart = new ShoppingCart();
$cartItems = $cart->getPositions();

$singleProduct = new \app\modules\MubAdmin\modules\furniture\models\Product();
$product = new \app\modules\MubAdmin\modules\furniture\models\Product();
$productImg = new \app\modules\MubAdmin\modules\furniture\models\ProductImages();
use  \app\modules\MubAdmin\modules\furniture\models\Category;

$productid = $products->id;
$urlData = Yii::$app->getRequest()->getQueryParam('id');

$procat = ProductCategories::find()->where(['del_status' => '0','product_id' => $productid])->one();
$proImg = ProductImages::find()->where(['del_status' => '0','product_id' => $productid])->all();


$procate = Category::find()->where(['del_status' => '0','category_slug' =>$urlData])->orderBy(['id'=>SORT_DESC])->all();
foreach ($procate as  $value) {
    $catId = $value['id'];
}
$productcat = ProductCategories::find()->where(['del_status' => '0','category_id' => $procat->category_id])->orderBy(['id'=>SORT_DESC])->limit(9)->all();
?>

<style type="text/css">.check {
    display: none!important;
}</style>
        <section class="main-header" style="background-image:url(/images/9.jpg)">
            <header>
                <div class="container">
                    <h1 class="h2 title"><?= $products->product_name;?></h1>
                    <ol class="breadcrumb breadcrumb-inverted">
                        <li><a href="#"><span class="icon icon-home"></span></a></li>
                        <li><a href="#">Product</a></li>
                        <li><a href="#"><?= $products->product_name;?></a></li>
                        <li><a class="active" href="#">Description</a></li>
                    </ol>
                </div>
            </header>
        </section>

        <!-- ========================  Product ======================== -->

        <section class="product">
            <div class="main">
                <div class="container">
                    <div class="row product-flex">

                        <!-- product flex is used only for mobile order -->
                        <!-- on mobile 'product-flex-info' goes bellow gallery 'product-flex-gallery' -->

                        <div class="col-md-4 col-sm-12 product-flex-info">
                            <div class="clearfix">

                                <!-- === product-title === -->

                                <h1 class="title" data-title="<?= $products->product_name;?>"><?= $products->product_name;?></small></h1>

                                <div class="clearfix">

                                    <!-- === price wrapper === -->

                                    <div class="price">
                                        <span class="h3">
                                            <?php if(!empty($products->mrp)) {?>
                                            ₹ <?= $products->mrp;}?>
                                            <!-- <small>₹ <?= $products->mrp;?></small> -->
                                        </span>
                                    </div>
                                    <hr />

                                    <!-- === info-box === -->

                                    <div class="info-box">
                                        <span><strong>Manufacture</strong></span>
                                        <span><?= $products->manufacture_SKU_name;?></span>
                                    </div>

                                    <!-- === info-box === -->

                                    <div class="info-box">
                                        <span><strong>Fabric Type</strong></span>
                                        <span><?= $products->fabric_type;?></span>
                                    </div>

                                    <!-- === info-box === -->

                                    <div class="info-box">
                                        <span><strong>Delivery Time</strong></span>
                                        <span><?= $products->time_to_ship;?></span>
                                    </div>

                                    <!-- === info-box === -->
                                    <div class="info-box">
                                        <span><strong>Arm Rest</strong></span>
                                        <span><?= $products->arm_rest;?></span>
                                    </div>

                                     <!-- === info-box === -->
                                    <div class="info-box">
                                        <span><strong>Seat Mattress</strong></span>
                                        <span><?= $products->seat_mattress;?></span>
                                    </div>

                                    <!-- === info-box === -->


                                    <div class="info-box">
                                        <span><strong>Availability</strong></span>
                                        <span><i class="fa fa-check-square-o"></i> In stock</span>
                                        <span class="hidden"><i class="fa fa-truck"></i> Out of stock</span>
                                    </div>

                                    <hr />
                                    <?php
                                            $alreadyInCart = $cart->getPositionById($products->id);
                                        ?>
                                   <button class="btn btn-danger colo btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $products->id?>" style="background: orange!important;"><span class="icon icon-cart cat"></span>Add To Cart</button>

                                    <button onclick="location.href = '/site/checkout'" class="btn btn-danger colo <?=($alreadyInCart)? '' :'check' ?>" id="checkout_<?= $products->id?>"><span class="icon icon-cart cat"></span>Checkout</button>

                                   <!-- <button class="btn btn-danger colo2"><span class="icon icon-cart cat"></span> Buy Now</button> -->

                                    <hr />

                                    <!-- === info-box === -->

                                    <div class="info-box">
                                        <h3>Specification</h3>

                                    </div>

                                    <!-- === info-box === -->

                                    <div class="info-box">
                                    </div>

                                </div> <!--/clearfix-->
                            </div> <!--/product-info-wrapper-->
                        </div> <!--/col-md-4-->
                        <!-- === product item gallery === -->

                        <div class="col-md-8 col-sm-12 product-flex-gallery">

                            <div class="owl-product-gallery open-popup-gallery">
                                <?php 
                                foreach ($proImg as $value) {
                                 $img = $value['thumbnail_url'];  
                                ?>
                                <a href="#"><img src="<?= ImageUploader::resizeRender($img, '750', '500'); ?>" alt="" /></a>
                                <?php 
                                 }?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- === product-info === -->

            <div class="info" style="margin-top: -3em;">
                <div class="container">
                    <div class="row">

                        <!-- === product-designer === -->

                        <div class="col-md-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <!-- <li role="presentation" class="active">
                                    <a href="#designer" aria-controls="designer" role="tab" data-toggle="tab">
                                        <i class="icon icon-user"></i>
                                        <span>Collection</span>
                                    </a>
                                </li> -->
                                <li role="presentation" class="active">
                                    <a href="#design" aria-controls="design" role="tab" data-toggle="tab">
                                        <i class="icon icon-sort-alpha-asc"></i>
                                        <span><b>Specification</b></span>
                                    </a>
                                </li>
                            </ul>

                            <!-- === tab-panes === -->

                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane active" id="design">
                                    <div class="content">

                                        <!-- === designer collection title === -->

                                        <div class="products">
                                            <div class="row">

                                            <div class="col-md-12" style="background-color: #e2e2e2;">
                                                <h5>Product Description</h5>
                                                <p style="margin-top: -1em;"><?= $products->description;?></p><br/>
                                            </div>

                                            <div class="col-md-12" style="background-color: #e2e2e2;">
                                                <h5>Furniture Materials</h5>
                                                <p style="margin-top: -1em;"><?= $products->furniture_material;?></p><br/>
                                            </div>

                                            <div class="col-md-12" style="background-color: #e2e2e2;">
                                                <h5>General Specification</h5>
                                                <p style="margin-top: -1em;"><?= $products->specification_general;?></p><br/>
                                            </div>
                                            

                                               

                                            </div> <!--/row-->
                                        </div> <!--/products-->
                                    </div> <!--/content-->
                                </div> <!--/tab-pane-->
                                <!-- ============ tab #2 ============ -->

                                <div role="tabpanel" class="tab-pane" id="rating">

                                    <!-- ============ ratings ============ -->

                                    <div class="content">
                                        <h3>Rating</h3>

                                        <div class="row">

                                            <!-- === comments === -->

                                            <div class="col-md-12">
                                                <div class="comments">

                                                    <!-- === rating === -->
                                                    <div class="comment-wrapper">

                                                        <!-- === comment === --><br/>

                                                        <div class="comment-block">
                                                            <div class="comment-user">
                                                                <div><img src="/images/user-2.jpg" alt="Alternate Text" width="70" /></div>
                                                                <div>
                                                                    <h5>
                                                                        <span>John Doe</span>
                                                                        <span class="pull-right">
                                                                            <i class="fa fa-star active"></i>
                                                                            <i class="fa fa-star active"></i>
                                                                            <i class="fa fa-star active"></i>
                                                                            <i class="fa fa-star active"></i>
                                                                            <i class="fa fa-star"></i>
                                                                        </span>
                                                                        <small>03.05.2017</small>
                                                                    </h5>
                                                                </div>
                                                            </div>

                                                            <!-- comment description -->

                                                            <div class="comment-desc">
                                                                <p>
                                                                    In vestibulum tellus ut nunc accumsan eleifend. Donec mattis cursus ligula, id
                                                                    iaculis dui feugiat nec. Etiam ut ante sed neque lacinia volutpat. Maecenas
                                                                    ultricies tempus nibh, sit amet facilisis mauris vulputate in. Phasellus
                                                                    tempor justo et mollis facilisis. Donec placerat at nulla sed suscipit. Praesent
                                                                    accumsan, sem sit amet euismod ullamcorper, justo sapien cursus nisl, nec
                                                                    gravida
                                                                </p>
                                                            </div>

                                                    <div class="comment-add">

                                                        <div class="comment-reply-message">
                                                            <div class="h3 title">Leave a Reply </div>
                                                            <p>Your email address will not be published.</p>
                                                        </div>

                                                        <form action="#" method="post">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="name" value="" placeholder="Your Name" />
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="name" value="" placeholder="Your Email" />
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea rows="10" class="form-control" placeholder="Your comment"></textarea>
                                                            </div>
                                                            <div class="clearfix text-center">
                                                                <a href="#" class="btn btn-main">Add comment</a>
                                                            </div>
                                                        </form>

                                                    </div><!--/comment-add-->
                                                </div> <!--/comments-->
                                            </div>


                                        </div> <!--/row-->
                                    </div> <!--/content-->
                                </div> <!--/tab-pane-->
                            </div> <!--/tab-content-->
                        </div>
                    </div> <!--/row-->
                </div> <!--/container-->
            </div> <!--/info-->
        </section>

        <!-- ========================  Product info popup - quick view ======================== -->
        <div class="container">
            <div class="row"><br/>
                <h1 class="text-center">Related Products</h1><br/>
                <div class="products">
                    <div class="row pad">

                        <!-- === product-item === -->
                        <?php 
                           foreach ($productcat as $value) { 
                            if($value['product_id'] !== $productid){
                            $proId = $value['product_id']; 
                            $productDetails = $product::find()->where(['del_status' => '0','id' => $proId])->one();
                            $relPro= $productDetails['id'];
                            $relImg = $productImg::find()->where(['del_status' => '0','product_id' => $relPro])->one();
                            ?>

                            <div class="col-md-3 col-xs-4">
                                <article>
                                    <div class="figure-grid">
                                        <div class="image">
                                            <a href="/site/product?name=<?= $productDetails['product_slug']?>">
                                                <?php if(!empty($relImg->thumbnail_url)){?>
                                                <img src="<?= ImageUploader::resizeRender($relImg->thumbnail_url, '260', '187'); ?>" alt="" />
                                                <?php }else{?>
                                <a href="/site/product?name=<?= $productDetails['product_slug']?>"><img src="/images/not-found.png" alt="" style="height: 195px;" /></a>
                                <?php }?>
                                            </a>
                                        </div>
                                         <!-- <div class="price">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span class="h3">$ 1999,00 <small>$ 2999,00</small></span>
                                        </div><br/> -->
                                        <div class="text" style="background-color: #dedede94;">
                                            <h4 class="title h4" style="font-size: 15px;"><a href="/site/product?name=<?= $productDetails['product_slug']?>"><?= $productDetails['product_name']?></a></h4>
                                            
                                            <sup>₹ <?= $productDetails['mrp']?></sup><br/><br/>
                                        </div>
                                    </div>
                                </article>
                            </div>
                         <?php } }?>

                        <br/><br/>

                    </div> <!--/row-->
                </div> <!--/products-->
            </div>
        </div>

        