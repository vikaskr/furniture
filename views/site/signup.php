<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">
    .product-advertise-form-inner input{
        padding: 15px 10px!important;
    }
    .modal-open .modal{
        background: none!important;
    }
    .modal-content{
    margin-top: 6em!important;
    background: #e6e6e6!important;
    width: 100%!important;
    }
    .product-advertise-form-inner input {
    padding: 8px 8px!important;
    width: 250px!important;
    }
    #myModal{
        margin-left: 4em!important;
    }

</style>

<button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
    <div class="login-popup-outer">
    <h3 class="magazines-by-sectors-heading" style="margin-bottom: 0px!important; background-color: #e6e6e6; padding: 20px; text-align: center; color: #cc9600!important;"><strong>REGISTER</strong></h3>
       <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => ['site/client-register-validate'],'options' => ['id' => 'frontend-signup','method' => 'POST','data-pjax' => true],'action' => ['/#']]); ?>
            <div class="col-xs-12 nopadding product-advertise-form">

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'First Name', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>  
                        <div class="col-md-6">
                        <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Last Name', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>  
                        <div class="col-md-6">
                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>
                </div>

                 <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'mobile')->textInput(['placeholder' => 'Mobile', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>   
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'address')->textInput(['placeholder' => 'Address', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                        <div class="col-md-6">
                         <?= $form->field($model, 'city')->textInput(['placeholder' => 'City', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>   
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'pin_code')->textInput(['placeholder' => 'Pincode/Zipcode', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'state')->textInput(['placeholder' => 'State', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>   
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button">
                    <input type="submit" name="Register" value="Register" class="btn btn-success" style="margin-left: 9em; margin-bottom: 1em;"">
                </div>
                <div class="col-xs-12 nopadding product-advertise-form-inner memnber-yet-con" style="text-align: center;"><span style="color: #000;"><br/>Already have a Account?  <a href="#" class="signup-user" id="signin-modal" style="color: #cc9600!important;">Login</a></span></div>
            </div>
        <?php ActiveForm::end(); ?>
</div>


<div class="modal-footer">  
</div>