<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $review app\modules\MubAdmin\modules\furniture\reviews\ReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($review, 'id') ?>

    <?= $form->field($review, 'product_id') ?>

    <?= $form->field($review, 'email') ?>

    <?= $form->field($review, 'name') ?>

    <?= $form->field($review, 'rating') ?>

    <?php // echo $form->field($review, 'review_id') ?>

    <?php // echo $form->field($review, 'read') ?>

    <?php // echo $form->field($review, 'title') ?>

    <?php // echo $form->field($review, 'type') ?>

    <?php // echo $form->field($review, 'comment_text') ?>

    <?php // echo $form->field($review, 'mub_user_id') ?>

    <?php // echo $form->field($review, 'status') ?>

    <?php // echo $form->field($review, 'approved_on') ?>

    <?php // echo $form->field($review, 'approved_by') ?>

    <?php // echo $form->field($review, 'created_at') ?>

    <?php // echo $form->field($review, 'updated_at') ?>

    <?php // echo $form->field($review, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
