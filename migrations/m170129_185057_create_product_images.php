<?php

namespace app\migrations;
use app\commands\Migration;

class m170129_185057_create_product_images extends Migration
{
    public function getTableName()
    {
        return 'product_images';
    }
    public function getForeignKeyFields()
    {
        return [
            'product_id' => ['product', 'id'],
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'product_id' => 'product_id',
            'mub_user_id' => 'mub_user_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->defaultValue(NULL),
            'title' => $this->string(),
            'thumbnail_url' => $this->string(100)->defaultValue(NULL),
            'thumbnail_path' => $this->string(100),
            'full_path' => $this->string(100)->notNull(),
            'visible' => "enum('0','1')",
            'keyword' => $this->string(),
            'width' => $this->integer(),
            'height' => $this->integer(),
            'display_width' => $this->integer(),
            'display_hieght' => $this->integer(),
            'type' => "enum('original','medium','thumbnail') NOT NULL DEFAULT 'original'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
