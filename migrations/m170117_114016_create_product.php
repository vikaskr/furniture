<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_114016_create_product extends Migration
{
    public function getTableName()
    {
        return 'product';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id' => 'mub_user_id',
            'product_name'  => 'product_name',
            'status' => 'status',                                      
            'created_at' => 'created_at'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'hsn_code' => $this->string(),
            'manufacture_SKU' => $this->string(),
            'manufacture_SKU_name' => $this->text(),
            'product_name' => $this->string()->notNull(),
            'product_slug' => $this->string()->notNull(),
            'description' => $this->text(),
            'inner_spec_of_seat' => $this->string(),
            'seat_mattress' => $this->string(),
            'arm_rest' => $this->string(),
            'stock_quantity' => $this->integer(),
            'base_cost' => $this->integer(),
            'sell_price' => $this->integer(),
            'mrp' => $this->integer()->notNull(),
            'time_to_ship' => $this->string(),
            'width' => $this->string(),
            'depth' => $this->string(),
            'height' => $this->string(),
            'product_group' => $this->string(),
            'furniture_material' => $this->string(),
            'fabric_type' => $this->string(),
            'foam_type_density' => $this->string(),
            'assembly' => $this->string(),
            'warranty' => $this->string(),
            'specification_general' => $this->string(),
            'furniture_weight' => $this->string(),
            'seating_height' => $this->string(),
            'box_content' => $this->string(),
            'box_count' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
