<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $rating app\modules\MubAdmin\modules\furniture\ratings\Rating */

$this->title = 'Update Rating: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Ratings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $rating->id, 'url' => ['view', 'id' => $rating->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rating-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'rating' => $rating,
    ]) ?>

</div>
